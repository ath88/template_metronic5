var FormControls = {
    init: function() {
        $("#form_form").validate({
            rules: {
                sgroupNama: {
                    required: !0
                },
                sgroupKeterangan: {
                    required: !0
                }
            },
            /*invalidHandler: function(e, r) {
                mApp.scrollTo("#form_form"), swal({
                    title: "",
                    text: "There are some errors in your submission. Please correct them.",
                    type: "error",
                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                })
            },*/
            submitHandler: function(e) {
                $('#response').html('');
                var button = $('#btn_button');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                $.ajax({
                    type: $(e).attr('method'),
                    url: $(e).attr('action'),
                    data: $(e).serialize(),
                    success: function(data) {
                        var res = $.parseJSON(data);
                        $('#response').fadeIn('slow').html(res.response);
                        swal({
                            position: "top-right",
                            type: res.status,
                            title: res.message,
                            showConfirmButton: !1,
                            timer: 1500
                        })
                        if(res.status=='success')
                        {
                            button.prop( "disabled", false );
                            button.removeClass('disabled');
                            button.text(button_text);             
                        }
                    }
                })
                return false
            }
        }), $(".ts_remove_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');
            swal({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function(e) {
                e.value && 
                    $.ajax(
                    {
                        url:$(idLink).attr('href'),
                        success:function(data) 
                        {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal("Deleted!", res.message, res.status) ;                   
                        }
                    });
            })    
        });
    }
};
jQuery(document).ready(function() {
    FormControls.init()
});