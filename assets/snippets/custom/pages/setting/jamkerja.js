var FormControls = {
    init: function() {
        $("#form_submit").validate({
            rules: {
                jkblnTahun: {
                    required: !0
                }
            },
            /*invalidHandler: function(e, r) {
                mApp.scrollTo("#form_form"), swal({
                    title: "",
                    text: "There are some errors in your submission. Please correct them.",
                    type: "error",
                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                })
            },*/
            submitHandler: function(e) {
                $('#response').html('');
                var button = $('#btn_button');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                $.ajax({
                    type: $(e).attr('method'),
                    url: $(e).attr('action'),
                    data: $(e).serialize(),
                    success: function(data) {
                        try {
                            var res = $.parseJSON(data);
                            var text = res.response;
                        }
                        catch (err) {
                            // Do something about the exception here
                            var text = data;
                        }
                        $('#response').fadeIn('slow').html(text);
                        button.prop( "disabled", false );
                        button.removeClass('disabled');
                        button.text(button_text); 
                        FormControls.init();    
                    }
                })
                return false
            }
        }), $("#form_form").validate({
            rules: {
            },
            /*invalidHandler: function(e, r) {
                mApp.scrollTo("#form_form"), swal({
                    title: "",
                    text: "There are some errors in your submission. Please correct them.",
                    type: "error",
                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                })
            },*/
            submitHandler: function(e) {
                $('#response').html('');
                var button = $('#btn_save');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                $.ajax({
                    type: $(e).attr('method'),
                    url: $(e).attr('action'),
                    data: $(e).serialize(),
                    success: function(data) {
                        var res = $.parseJSON(data);
                        $('#response').fadeIn('slow').html(res.response);
                        swal({
                            position: "top-right",
                            type: res.status,
                            title: res.message,
                            showConfirmButton: !1,
                            timer: 1500
                        })
                        if(res.status=='success')
                        {
                            button.prop( "disabled", false );
                            button.removeClass('disabled');
                            button.text(button_text);             
                        }
                    }
                })
                return false
            }
        }), $(".m-select2").select2({
                placeholder: "Pilih...",
                allowClear: !0
        });
    }
};
jQuery(document).ready(function() {
    FormControls.init()
});