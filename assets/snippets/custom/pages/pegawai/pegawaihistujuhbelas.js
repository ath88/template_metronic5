var FormControls = {
    init: function() {
        $("#form_submit").validate({
            rules: {
                unitId: {
                    required: !0
                },
                pegNIP: {
                    required: !0
                }
            },
            /*invalidHandler: function(e, r) {
                mApp.scrollTo("#form_form"), swal({
                    title: "",
                    text: "There are some errors in your submission. Please correct them.",
                    type: "error",
                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                })
            },*/
            submitHandler: function(e) {
                $('#response').html('');
                var button = $('#btn_button');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                $.ajax({
                    type: $(e).attr('method'),
                    url: $(e).attr('action'),
                    data: $(e).serialize(),
                    success: function(data) {
                        try {
                            var res = $.parseJSON(data);
                            var text = res.response;
                        }
                        catch (err) {
                            // Do something about the exception here
                            var text = data;
                        }
                        $('#response').fadeIn('slow').html(text);
                        button.prop( "disabled", false );
                        button.removeClass('disabled');
                        button.text(button_text); 
                        FormControls.init();    
                    }
                })
                return false
            }
        }), $("#form_form").validate({
            rules: {
                pegNIP: {
                    required: !0
                },
                pegNama: {
                    required: !0
                },
                pegGol: {
                    required: !0
                },
                pegKodeUnitKerja: {
                    required: !0
                },
                jjab: {
                    required: !0
                },
                pegJabId: {
                    required: !0
                },
                pegTMT: {
                    required: !0
                }
            },
            /*invalidHandler: function(e, r) {
                mApp.scrollTo("#form_form"), swal({
                    title: "",
                    text: "There are some errors in your submission. Please correct them.",
                    type: "error",
                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                })
            },*/
            submitHandler: function(e) {
                $('#response').html('');
                var button = $('#btn_save');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                $.ajax({
                    type: $(e).attr('method'),
                    url: $(e).attr('action'),
                    data: $(e).serialize(),
                    success: function(data) {
                        var res = $.parseJSON(data);
                        $('#response').fadeIn('slow').html(res.response);
                        swal({
                            position: "top-right",
                            type: res.status,
                            title: res.message,
                            showConfirmButton: !1,
                            timer: 1500
                        })
                        if(res.status=='success')
                        {
                            button.prop( "disabled", false );
                            button.removeClass('disabled');
                            button.text(button_text);             
                        }
                    }
                })
                return false
            }
        }), $(".ts_remove_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');
            swal({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function(e) {
                e.value && 
                    $.ajax(
                    {
                        url:$(idLink).attr('href'),
                        success:function(data) 
                        {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal("Deleted!", res.message, res.status) ;                   
                        }
                    });
            })    
        }), $(".m-select2").select2({
                placeholder: "Pilih...",
                allowClear: !0
        }), $(".datepick").datepicker({
            autoclose: !0,
            todayHighlight: !0,
            orientation: "bottom left",
            format: 'yyyy-mm-dd',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }), $("#jjab").change(function(e) {
            $('#pegJabId').val('');
            $('#pegJabId').html('');
            $('#pegJabId').addClass('m-loader m-loader--brand');
            var jjabId = $("#jjab").val();
            $.ajax({
                type:'post',
                url:'/pegawaihistujuhbelas/loadjabatan',
                data:'jjabId='+jjabId,
                success:function(data)
                {
                    $('#pegJabId').html(data);
                    $('#pegJabId').removeClass('disabled');
                }
            });
            return false;  
        });
    }
};
jQuery(document).ready(function() {
    FormControls.init()
});