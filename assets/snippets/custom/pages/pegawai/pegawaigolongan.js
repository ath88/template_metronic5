var FormControls = {
    init: function() {
        $("#form_submit").validate({
            rules: {
                unitId: {
                    required: !0
                },
                pegNIP: {
                    required: !0
                }
            },
            /*invalidHandler: function(e, r) {
                mApp.scrollTo("#form_form"), swal({
                    title: "",
                    text: "There are some errors in your submission. Please correct them.",
                    type: "error",
                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                })
            },*/
            submitHandler: function(e) {
                $('#response').html('');
                var button = $('#btn_button');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                $.ajax({
                    type: $(e).attr('method'),
                    url: $(e).attr('action'),
                    data: $(e).serialize(),
                    success: function(data) {
                        try {
                            var res = $.parseJSON(data);
                            var text = res.response;
                        }
                        catch (err) {
                            // Do something about the exception here
                            var text = data;
                        }
                        $('#response').fadeIn('slow').html(text);
                        button.prop( "disabled", false );
                        button.removeClass('disabled');
                        button.text(button_text); 
                        FormControls.init();    
                    }
                })
                return false
            }
        }), $("#form_form").validate({
            rules: {
                pegGolNomorKtp: {
                    required: !0
                },
                pegGolGolonganKode: {
                    required: !0
                },
                pegGolNoSK: {
                    required: !0
                },
                pegGolTanggalSK: {
                    required: !0
                },
                pegGolTmt: {
                    required: !0
                }
            },
            /*invalidHandler: function(e, r) {
                mApp.scrollTo("#form_form"), swal({
                    title: "",
                    text: "There are some errors in your submission. Please correct them.",
                    type: "error",
                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                })
            },*/
            submitHandler: function(e) {
                $('#response').html('');
                var button = $('#btn_save');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');

                var form = $('#form_form')[0];
                var file_data = $('#customFile').prop('files')[0];   
                var form_data = new FormData(form);                  
                form_data.append('file', file_data);
                
                $.ajax({
                    type: $(e).attr('method'),
                    url: $(e).attr('action'),
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        var res = $.parseJSON(data);
                        $('#response').fadeIn('slow').html(res.response);
                        swal({
                            position: "top-right",
                            type: res.status,
                            title: res.message,
                            showConfirmButton: !1,
                            timer: 1500
                        })
                        if(res.status=='success')
                        {
                            button.prop( "disabled", false );
                            button.removeClass('disabled');
                            button.text(button_text);             
                        }
                    }
                })
                return false
            }
        }), $(".ts_remove_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');
            swal({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function(e) {
                e.value && 
                    $.ajax(
                    {
                        url:$(idLink).attr('href'),
                        success:function(data) 
                        {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal("Deleted!", res.message, res.status) ;                   
                        }
                    });
            })    
        }), $(".m-select2").select2({
                placeholder: "Pilih...",
                allowClear: !0
        }), $(".datepick").datepicker({
            autoclose: !0,
            todayHighlight: !0,
            orientation: "bottom left",
            format: 'yyyy-mm-dd',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
    }
};
jQuery(document).ready(function() {
    FormControls.init()
});