var FormControls = {
    init: function() {
        $("#form_submit").validate({
            rules: {
                unitId: {
                    pegCari: !0
                }
            },
            /*invalidHandler: function(e, r) {
                mApp.scrollTo("#form_form"), swal({
                    title: "",
                    text: "There are some errors in your submission. Please correct them.",
                    type: "error",
                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                })
            },*/
            submitHandler: function(e) {
                $('#response').html('');
                var button = $('#btn_button');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                $.ajax({
                    type: $(e).attr('method'),
                    url: $(e).attr('action'),
                    data: $(e).serialize(),
                    success: function(data) {
                        try {
                            var res = $.parseJSON(data);
                            var text = res.response;
                        }
                        catch (err) {
                            // Do something about the exception here
                            var text = data;
                        }
                        $('#response').fadeIn('slow').html(text);
                        button.prop( "disabled", false );
                        button.removeClass('disabled');
                        button.text(button_text); 
                        FormControls.init();    
                    }
                })
                return false
            }
        }), $(".ts_reset_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');
            swal({
                title: "Apakah Anda Yakin Akan Reset Password?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Reset!"
            }).then(function(e) {
                e.value && 
                    $.ajax(
                    {
                        url:$(idLink).attr('href'),
                        success:function(data) 
                        {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal("Reset!", res.message, res.status) ;                   
                        }
                    });
            })    
        }), $(".m-select2").select2({
                placeholder: "Pilih...",
                allowClear: !0
            });
    }
};
jQuery(document).ready(function() {
    FormControls.init()
});