var FormLoadPegawai = {
    init: function() {
        $("#unitId").change(function(e) {
            $('#NIP').val('');
            $('#NIP').html('');
            $('#NIP').addClass('m-loader m-loader--brand');
            var unitId = $("#unitId").val();
            $.ajax({
                type:'post',
                url:'/pegawaijabatan/loadpegawai',
                data:'unitid='+unitId,
                success:function(data)
                {
                    $('#NIP').html(data);
                    $('#NIP').removeClass('disabled');
                }
            });
            return false;  
        })
    }
};
jQuery(document).ready(function() {
    FormLoadPegawai.init()
});