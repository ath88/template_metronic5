var SnippetLogin = function() {
    var s = $("#m_login"),
        n = function(e, i, a) {
            var l = $('<div class="m-alert m-alert--outline alert alert-' + i + ' alert-dismissible" role="alert">\t\t\t<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\t\t\t<span></span>\t\t</div>');
            e.find(".alert").remove(), l.prependTo(e), mUtil.animateClass(l[0], "fadeIn animated"), l.find("span").html(a)
        };
    return {
        init: function() {
            $("#m_login_signin_submit").click(function(e) {
                e.preventDefault();
                var t = $(this),
                    r = $(this).closest("form");
                r.validate({
                    rules: {
                        username: {
                            required: !0
                        },
                        password: {
                            required: !0
                        }
                    }
                }), r.valid() && (t.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), r.ajaxSubmit({
                    type: r.attr('method'),
                    url: r.attr('action'),
                    data: r.serialize(),
                    success: function(e, i, a, l) {
                    	var eR = $.parseJSON(e);
                        setTimeout(function() {
                            t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), n(r, eR.status, eR.message)
                        }, 1e3)
                        if(eR.status=='success')
                        {
                        	setTimeout(function(){
						        window.location.href = eR.redirect_url;
						    }, 2e3);
                        	
                        }
                    }
                }))
            })
        }
    }
}();
jQuery(document).ready(function() {
    SnippetLogin.init()
});