var FormControls = {
    init: function() {
        $("#form_form").validate({
            rules: {
                HAKAKSES: {
                    required: !0
                }
            },
            submitHandler: function(e) {
                $('#response').html('');
                var button = $('#btn_save');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                $.ajax({
                    type: $(e).attr('method'),
                    url: $(e).attr('action'),
                    data: $(e).serialize(),
                    success: function(data) {
                        var res = $.parseJSON(data);
                        $('#response').fadeIn('slow').html(res.response);
                        swal({
                            position: "top-right",
                            type: res.status,
                            title: res.message,
                            showConfirmButton: !1,
                            timer: 1500
                        })
                        button.prop( "disabled", false );
                        button.removeClass('disabled');
                        button.text(button_text);    

                    }
                })
                return false
            }
        }), $(".m-select2").select2({
                placeholder: "Pilih...",
                allowClear: !0
            });
    }
};
jQuery(document).ready(function() {
    FormControls.init()
});