var Dashboard = {
    init: function() {
    	if (0 != $("#m_dashboard_daterangepicker").length) {
            var n = $("#m_dashboard_daterangepicker"),
                e = moment(),
                t = moment();
            n.daterangepicker({
                startDate: e,
                endDate: t,
                opens: "left",
                ranges: {
                    Today: [moment(), moment()],
                    Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "Last 7 Days": [moment().subtract(6, "days"), moment()],
                    "Last 30 Days": [moment().subtract(29, "days"), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                }
            }, a), a(e, t, "")
        }

        function a(e, t, a) {
            var r = "",
                o = "";
            t - e < 100 ? (r = "Today:", o = e.format("MMM D")) : "Yesterday" == a ? (r = "Yesterday:", o = e.format("MMM D")) : o = e.format("MMM D") + " - " + t.format("MMM D"), n.find(".m-subheader__daterange-date").html(o), n.find(".m-subheader__daterange-title").html(r)
        }
    	
        if (0 != $("#m_chart_profit_share").length) {
            var e = new Chartist.Pie("#m_chart_profit_share", {
                series: [{
                    value: $('#DS').attr('tagvalue'),
                    className: "custom",
                    meta: {
                        color: mApp.getColor("brand")
                    }
                }, {
                    value: $('#DT').attr('tagvalue'),
                    className: "custom",
                    meta: {
                        color: mApp.getColor("accent")
                    }
                }, {
                    value: $('#TKS').attr('tagvalue'),
                    className: "custom",
                    meta: {
                        color: mApp.getColor("warning")
                    }
                }, {
                    value: $('#TKF').attr('tagvalue'),
                    className: "custom",
                    meta: {
                        color: mApp.getColor("success")
                    }
                }],
                labels: [1, 2, 3, 4]
            }, {
                donut: !0,
                donutWidth: 17,
                showLabel: !1
            });
            e.on("draw", function(e) {
                if ("slice" === e.type) {
                    var t = e.element._node.getTotalLength();
                    e.element.attr({
                        "stroke-dasharray": t + "px " + t + "px"
                    });
                    var a = {
                        "stroke-dashoffset": {
                            id: "anim" + e.index,
                            dur: 1e3,
                            from: -t + "px",
                            to: "0px",
                            easing: Chartist.Svg.Easing.easeOutQuint,
                            fill: "freeze",
                            stroke: e.meta.color
                        }
                    };
                    0 !== e.index && (a["stroke-dashoffset"].begin = "anim" + (e.index - 1) + ".end"), e.element.attr({
                        "stroke-dashoffset": -t + "px",
                        stroke: e.meta.color
                    }), e.element.animate(a, !1)
                }
            }), e.on("created", function() {
                window.__anim21278907124 && (clearTimeout(window.__anim21278907124), window.__anim21278907124 = null), window.__anim21278907124 = setTimeout(e.update.bind(e), 15e3)
            })
        }
    }
};
jQuery(document).ready(function() {
    Dashboard.init()
});