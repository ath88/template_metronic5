/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.28-MariaDB : Database - crowdresearch
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`crowdresearch` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `crowdresearch`;

/*Table structure for table `d_arsip` */

DROP TABLE IF EXISTS `d_arsip`;

CREATE TABLE `d_arsip` (
  `arsipId` int(11) NOT NULL AUTO_INCREMENT,
  `arsipTanggal` date DEFAULT NULL,
  `arsipKode` varchar(50) DEFAULT NULL,
  `arsipKegiatan` varchar(100) DEFAULT NULL,
  `arsipTanggalKegiatan` date DEFAULT NULL,
  `arsipPelaksanaan` varchar(100) DEFAULT NULL,
  `arsipBukti` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`arsipId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `d_arsip` */

/*Table structure for table `d_pegawai` */

DROP TABLE IF EXISTS `d_pegawai`;

CREATE TABLE `d_pegawai` (
  `pegNip` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'NIP Pegawai (PK)',
  `pegNama` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Nama Pegawai',
  `pegNomorKtp` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nomor KTP Pegawai',
  `pegGelarDepan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Gelar Depan Pegawai',
  `pegGelarBelakang` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Gelar Belakang Pegawai',
  `pegKotaLahir` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ID Kota Lahir Pegawai',
  `pegTanggalLahir` date DEFAULT NULL COMMENT 'Tanggal Lahir Pegawai',
  `pegJenisKelamin` enum('L','P') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Jenis Kelamin Pegawai, L: Laki-Laki, P: Perempuan',
  `pegGolonganDarah` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Golongan Darah Pegawai',
  `pegAgmrId` smallint(6) DEFAULT NULL COMMENT 'ID Agama',
  `pegStnkrId` smallint(6) DEFAULT NULL COMMENT 'Status Nikah',
  `pegAlamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Alamat Rumah Pegawai',
  `pegNoHP` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'No. Handphone',
  `pegEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Email',
  `pegIsAktif` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Status Aktf Pegawai, 0: Non Aktif, 1: Aktif',
  `pegTanggalPengubahan` date DEFAULT NULL COMMENT 'Tanggal Pengubah Data',
  `pegUnitId` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`pegNip`,`pegNomorKtp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `d_pegawai` */

insert  into `d_pegawai`(`pegNip`,`pegNama`,`pegNomorKtp`,`pegGelarDepan`,`pegGelarBelakang`,`pegKotaLahir`,`pegTanggalLahir`,`pegJenisKelamin`,`pegGolonganDarah`,`pegAgmrId`,`pegStnkrId`,`pegAlamat`,`pegNoHP`,`pegEmail`,`pegIsAktif`,`pegTanggalPengubahan`,`pegUnitId`) values 
('197003191997031001','Jarwoko','197003191997031001','Dr','M.Pd','-','2018-06-14','L','A',1,2,'Q','1','aa@a.com',1,NULL,1);

/*Table structure for table `d_pegawai_jabatan` */

DROP TABLE IF EXISTS `d_pegawai_jabatan`;

CREATE TABLE `d_pegawai_jabatan` (
  `pegJabId` bigint(20) NOT NULL AUTO_INCREMENT,
  `pegJabNomorKtp` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nomor KTP Pegawai',
  `pegJabNoSK` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'No SK PNS',
  `pegJabTanggalSK` date NOT NULL,
  `pegJabJabatanId` smallint(6) NOT NULL COMMENT 'Jabatan Pegawai',
  `pegJabTMT` date NOT NULL COMMENT 'Tmt Jabatan Pegawai',
  PRIMARY KEY (`pegJabId`,`pegJabNomorKtp`,`pegJabNoSK`,`pegJabJabatanId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `d_pegawai_jabatan` */

insert  into `d_pegawai_jabatan`(`pegJabId`,`pegJabNomorKtp`,`pegJabNoSK`,`pegJabTanggalSK`,`pegJabJabatanId`,`pegJabTMT`) values 
(5,'197003191997031001','112233','2018-06-14',3,'2018-06-14');

/*Table structure for table `d_pegawai_pangkat` */

DROP TABLE IF EXISTS `d_pegawai_pangkat`;

CREATE TABLE `d_pegawai_pangkat` (
  `pegGolId` bigint(20) NOT NULL AUTO_INCREMENT,
  `pegGolNomorKtp` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nomor KTP Pegawai',
  `pegGolNoSK` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'No SK PNS',
  `pegGolTanggalSK` date NOT NULL COMMENT 'Tanggal SK PNS',
  `pegGolGolonganKode` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Golongan Pegawai',
  `pegGolTmt` date NOT NULL COMMENT 'Tmt Golongan Pegawai',
  PRIMARY KEY (`pegGolId`,`pegGolNomorKtp`,`pegGolNoSK`,`pegGolGolonganKode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `d_pegawai_pangkat` */

insert  into `d_pegawai_pangkat`(`pegGolId`,`pegGolNomorKtp`,`pegGolNoSK`,`pegGolTanggalSK`,`pegGolGolonganKode`,`pegGolTmt`) values 
(2,'197003191997031001','112233','2018-06-14','IV/b','2018-06-14');

/*Table structure for table `d_surat_keluar` */

DROP TABLE IF EXISTS `d_surat_keluar`;

CREATE TABLE `d_surat_keluar` (
  `suratkeluarId` int(11) NOT NULL AUTO_INCREMENT,
  `suratkeluarNomor` varchar(50) DEFAULT NULL,
  `suratkeluarTanggalBuat` date DEFAULT NULL,
  `suratkeluarTujuan` varchar(100) DEFAULT NULL,
  `suratkeluarDisposisi` varchar(50) DEFAULT NULL,
  `suratkeluarPerihal` varchar(100) DEFAULT NULL,
  `suratkeluarNomorAgenda` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`suratkeluarId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `d_surat_keluar` */

/*Table structure for table `d_surat_masuk` */

DROP TABLE IF EXISTS `d_surat_masuk`;

CREATE TABLE `d_surat_masuk` (
  `suratmasukId` int(11) NOT NULL AUTO_INCREMENT,
  `suratmasukNomor` varchar(50) DEFAULT NULL,
  `suratmasukTanggalDiterima` date DEFAULT NULL,
  `suratmasukTanggalDiproses` date DEFAULT NULL,
  `suratmasukTanggalDisposisi` date DEFAULT NULL,
  `suratmasukUnit` smallint(6) DEFAULT NULL,
  `suratmasukPerihal` varchar(100) DEFAULT NULL,
  `suratmasukNomorAgenda` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`suratmasukId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `d_surat_masuk` */

/*Table structure for table `ref_agama` */

DROP TABLE IF EXISTS `ref_agama`;

CREATE TABLE `ref_agama` (
  `agmrId` smallint(6) NOT NULL DEFAULT '0' COMMENT 'ID Agama (PK)',
  `agmrNama` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Nama Agama',
  PRIMARY KEY (`agmrId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ref_agama` */

insert  into `ref_agama`(`agmrId`,`agmrNama`) values 
(1,'ISLAM'),
(2,'KATHOLIK'),
(3,'PROTESTAN'),
(4,'HINDU'),
(5,'BUDHA'),
(6,'KONGHUCHU');

/*Table structure for table `ref_golongan` */

DROP TABLE IF EXISTS `ref_golongan`;

CREATE TABLE `ref_golongan` (
  `golonganKode` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `golonganUraian` varchar(35) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`golonganKode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Master Tabel Golongan Ruang (Pangkat)';

/*Data for the table `ref_golongan` */

insert  into `ref_golongan`(`golonganKode`,`golonganUraian`) values 
('I/a','Juru Muda                          '),
('I/b','Juru Muda Tk. I                    '),
('I/c','J u r u                            '),
('I/d','Juru Tk. I                         '),
('II/a','Pengatur Muda                      '),
('II/b','Pengatur Muda Tk. I                '),
('II/c','Pengatur                           '),
('II/d','Pengatur Tk. I                     '),
('III/a','Penata Muda                        '),
('III/b','Penata Muda Tk .I                  '),
('III/c','Penata                             '),
('III/d','Penata Tk. I                       '),
('IV/a','Pembina                            '),
('IV/b','Pembina Tk. I                      '),
('IV/c','Pembina Utama Muda                 '),
('IV/d','Pembina Utama Madya                '),
('IV/e','Pembina Utama                      ');

/*Table structure for table `ref_jabatan` */

DROP TABLE IF EXISTS `ref_jabatan`;

CREATE TABLE `ref_jabatan` (
  `jabatanId` int(11) NOT NULL AUTO_INCREMENT,
  `jabatanUraian` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`jabatanId`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `ref_jabatan` */

insert  into `ref_jabatan`(`jabatanId`,`jabatanUraian`) values 
(3,'Kepala'),
(4,'Kepala Subbagian Umum'),
(5,'Kepala Seksi SFPMP'),
(6,'Kepala Seksi SIPM'),
(7,'Analis Pelaksana Program dan Anggaran'),
(8,'Bendahara Pengeluaran'),
(9,'Analis Barang Milik Negara'),
(10,'Bendahara Pengeluaran Pembantu'),
(11,'Penyusun Program'),
(12,'Pengolah Sistem dan Jaringan'),
(13,'Penyusun Program dan Anggaran'),
(14,'Penata Urusan Pimpinan'),
(15,'Pengadministrasian BMN'),
(16,'Penata Dokumen Keuangan'),
(17,'Pengadministrasian Persuratan'),
(18,'Teknisi Sarana dan Prasarana Kantor'),
(19,'Penyusun Laporan Keuangan'),
(20,'Pengolah Data Ketatalaksanaan'),
(21,'Pengolah Data Kepegawaian'),
(22,'Penata Dokumen'),
(23,'Tenaga Keamanan'),
(24,'Pengadministrasian Umum'),
(25,'Pengolah Data Program'),
(26,'Penyusun Publikasi & Informasi'),
(27,'Pengelola Data Sistem Informasi');

/*Table structure for table `ref_pendidikan` */

DROP TABLE IF EXISTS `ref_pendidikan`;

CREATE TABLE `ref_pendidikan` (
  `pendidikanId` int(11) NOT NULL AUTO_INCREMENT,
  `pendidikanNama` varchar(10) DEFAULT NULL,
  `pendidikanKeterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pendidikanId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `ref_pendidikan` */

insert  into `ref_pendidikan`(`pendidikanId`,`pendidikanNama`,`pendidikanKeterangan`) values 
(1,'SD','Sekolah Dasar'),
(2,'SMP','Sekolah Menengah Pertama'),
(3,'SMA','Sokolah Menengah Atas'),
(4,'D1','Diploma I'),
(5,'D2','Diploma II'),
(6,'D3','Diploma III'),
(7,'D4','Diploma IV'),
(8,'S1','Sarjana'),
(9,'S2','Magister'),
(10,'S3','Doktor');

/*Table structure for table `ref_status_nikah` */

DROP TABLE IF EXISTS `ref_status_nikah`;

CREATE TABLE `ref_status_nikah` (
  `stnkrId` smallint(6) NOT NULL DEFAULT '0' COMMENT 'ID Status Nikah',
  `stnkrNama` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Nama Status Nikah',
  PRIMARY KEY (`stnkrId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ref_status_nikah` */

insert  into `ref_status_nikah`(`stnkrId`,`stnkrNama`) values 
(1,'Belum Kawin'),
(2,'Sudah Kawin'),
(3,'Janda / Duda'),
(4,'Suami/Istri Meninggal');

/*Table structure for table `s_unit` */

DROP TABLE IF EXISTS `s_unit`;

CREATE TABLE `s_unit` (
  `unitId` int(11) NOT NULL AUTO_INCREMENT,
  `unitKode` varchar(10) NOT NULL,
  `unitNama` varchar(75) NOT NULL,
  `unitFakKode` int(11) DEFAULT NULL,
  PRIMARY KEY (`unitId`),
  UNIQUE KEY `unitKode` (`unitKode`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `s_unit` */

insert  into `s_unit`(`unitId`,`unitKode`,`unitNama`,`unitFakKode`) values 
(1,'01','LPMP',NULL),
(2,'02','Sub Bagian Umum',NULL),
(3,'03','Seksi SIPM',NULL),
(4,'04','Seksi SFPMP',NULL);

/*Table structure for table `s_user` */

DROP TABLE IF EXISTS `s_user`;

CREATE TABLE `s_user` (
  `susrNama` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'ID User',
  `susrPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Password User',
  `susrSgroupNama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Group User',
  `susrProfil` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nama User',
  `susrPertanyaan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '?',
  `susrJawaban` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '?',
  `susrAvatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Path File',
  `susrRefIndex` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Ref Index',
  `susrLastLogin` datetime DEFAULT NULL,
  `susrReset` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `susrResetTimestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`susrNama`),
  KEY `susrSgroupNama` (`susrSgroupNama`),
  CONSTRAINT `s_user_ibfk_1` FOREIGN KEY (`susrSgroupNama`) REFERENCES `s_user_group` (`sgroupNama`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `s_user` */

insert  into `s_user`(`susrNama`,`susrPassword`,`susrSgroupNama`,`susrProfil`,`susrPertanyaan`,`susrJawaban`,`susrAvatar`,`susrRefIndex`,`susrLastLogin`,`susrReset`,`susrResetTimestamp`) values 
('admin','1879d3aa09f88311e55e9f3702659fdc','ADMIN','ADMINISTRATOR',NULL,NULL,NULL,NULL,'2018-04-19 08:47:49',NULL,NULL),
('dwi','423196d16dad11e060176b52738947e3','ADMIN','Dwi rachmawati',NULL,NULL,NULL,NULL,'2018-01-17 11:09:51',NULL,NULL);

/*Table structure for table `s_user_group` */

DROP TABLE IF EXISTS `s_user_group`;

CREATE TABLE `s_user_group` (
  `sgroupNama` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Nama Group user',
  `sgroupKeterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Keterangan Group User',
  PRIMARY KEY (`sgroupNama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `s_user_group` */

insert  into `s_user_group`(`sgroupNama`,`sgroupKeterangan`) values 
('ADMIN','ADMINISTRATOR');

/*Table structure for table `s_user_group_modul` */

DROP TABLE IF EXISTS `s_user_group_modul`;

CREATE TABLE `s_user_group_modul` (
  `sgroupmodulSgroupNama` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Nama Group User',
  `sgroupmodulSusrmodulNama` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Nama Modul',
  `sgroupmodulSusrmodulRead` int(11) DEFAULT '1' COMMENT 'Status Read User, 0: Tidak, 1: Tampil',
  PRIMARY KEY (`sgroupmodulSgroupNama`,`sgroupmodulSusrmodulNama`),
  KEY `sgroupmodulSusrmodulNama` (`sgroupmodulSusrmodulNama`),
  CONSTRAINT `s_user_group_modul_ibfk_2` FOREIGN KEY (`sgroupmodulSusrmodulNama`) REFERENCES `s_user_modul_ref` (`susrmodulNama`) ON UPDATE CASCADE,
  CONSTRAINT `s_user_group_modul_ibfk_3` FOREIGN KEY (`sgroupmodulSgroupNama`) REFERENCES `s_user_group` (`sgroupNama`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `s_user_group_modul` */

insert  into `s_user_group_modul`(`sgroupmodulSgroupNama`,`sgroupmodulSusrmodulNama`,`sgroupmodulSusrmodulRead`) values 
('ADMIN','googlescholar',1),
('ADMIN','hakakses',1),
('ADMIN','hakaksesmodul',1),
('ADMIN','hakaksesunit',1),
('ADMIN','jabatan',1),
('ADMIN','pegawai',1),
('ADMIN','pegawaigolongan',1),
('ADMIN','pegawaijabatan',1),
('ADMIN','pengguna',1);

/*Table structure for table `s_user_group_unit` */

DROP TABLE IF EXISTS `s_user_group_unit`;

CREATE TABLE `s_user_group_unit` (
  `sgroupunitSgroupNama` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Nama Group User',
  `sgroupunitUnitId` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Nama Modul',
  `sgroupunitUnitRead` int(11) DEFAULT '1' COMMENT 'Status Read User, 0: Tidak, 1: Tampil',
  PRIMARY KEY (`sgroupunitSgroupNama`,`sgroupunitUnitId`),
  KEY `sgroupmodulSusrmodulNama` (`sgroupunitUnitId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `s_user_group_unit` */

insert  into `s_user_group_unit`(`sgroupunitSgroupNama`,`sgroupunitUnitId`,`sgroupunitUnitRead`) values 
('ADMIN','1',1),
('ADMIN','2',1),
('ADMIN','3',1),
('ADMIN','4',1);

/*Table structure for table `s_user_modul_group_ref` */

DROP TABLE IF EXISTS `s_user_modul_group_ref`;

CREATE TABLE `s_user_modul_group_ref` (
  `susrmdgroupNama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `susrmdgroupDisplay` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `susrmdgroupIcon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`susrmdgroupNama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `s_user_modul_group_ref` */

insert  into `s_user_modul_group_ref`(`susrmdgroupNama`,`susrmdgroupDisplay`,`susrmdgroupIcon`) values 
('admin','Administrator','flaticon-imac'),
('apitest','API Testing','flaticon-star'),
('pegawai','Pegawai','flaticon-avatar'),
('setting','Setting','flaticon-settings');

/*Table structure for table `s_user_modul_ref` */

DROP TABLE IF EXISTS `s_user_modul_ref`;

CREATE TABLE `s_user_modul_ref` (
  `susrmodulNama` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Nama Modul',
  `susrmodulNamaDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Display Modul',
  `susrmodulSusrmdgroupNama` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `susrmodulIsLogin` tinyint(4) DEFAULT '1',
  `susrmodulUrut` int(11) DEFAULT NULL,
  PRIMARY KEY (`susrmodulNama`),
  KEY `susrmodulSusrmdgroupNama` (`susrmodulSusrmdgroupNama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='modul yg bisa diakses oleh group';

/*Data for the table `s_user_modul_ref` */

insert  into `s_user_modul_ref`(`susrmodulNama`,`susrmodulNamaDisplay`,`susrmodulSusrmdgroupNama`,`susrmodulIsLogin`,`susrmodulUrut`) values 
('googlescholar','Google Scholar','apitest',1,NULL),
('hakakses','Hak Akses','admin',1,NULL),
('hakaksesmodul','Hak Akses Modul','admin',1,NULL),
('hakaksesunit','Hak Akses Unit','admin',1,NULL),
('jabatan','Jabatan','setting',1,NULL),
('login','Login',NULL,0,3),
('pegawai','Master Pegawai','pegawai',1,NULL),
('pegawaigolongan','History Golongan','pegawai',1,NULL),
('pegawaijabatan','History Jabatan','pegawai',1,NULL),
('pengguna','Pengguna','admin',1,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
