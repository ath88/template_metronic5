
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('pegawaihisjabbulan'))
{
	function pegawaihisjabbulan($pegNIP,$tahun="")
	{
	    // Get a reference to the controller object
	    $CI = get_instance();

	    // You may need to load the model if it hasn't been pre-loaded
		$CI->load->model('model_pegawaihistorybulan','',TRUE);
		$CI->load->model('model_master','',TRUE);

		if(empty($tahun))
		{
			$tahunAktif = $CI->model_master->get_by_id('r_tahun',array("tahunIsAktif"=>1));
			$tahun = $tahunAktif->tahun;
		}


		//DELETE DATA JABATAN BULAN TAHUN AKTIF
		$CI->model_master->delete('d_pegawai_history_jabatan_bulan',array("pegbNIP"=>$pegNIP,"pegbTahun"=>$tahun));

		for($i=1;$i<=12;$i++)
		{
			$bulan = substr('0'.$i, -2);
			$tmtawal = $tahun.'-'.$bulan.'-01';
			$tmtakhir = date("Y-m-t", strtotime($tmtawal));

			$param = false;

			$jabatan = $CI->model_pegawaihistorybulan->get_jabatan($pegNIP,$tmtawal,$tmtakhir);
			//echo $CI->db->last_query().'<br/>';
			if($jabatan!=false)
			{
				if(count($jabatan)==1)
				{
					if(strtotime($jabatan[0]->pegTMT) > strtotime($tmtawal))
						$tmtawal = $jabatan[0]->pegTMT;
					if(strtotime($jabatan[0]->pegTMTakhir) < strtotime($tmtakhir) and $jabatan[0]->pegTMTakhir != '0000-00-00')
						$tmtakhir = $jabatan[0]->pegTMTakhir;
					$param = array(
		    			'pegbNIP'=>$pegNIP,
		    			'pegbBulan'=>$bulan,
		    			'pegbTahun'=>$tahun,
		    			'pegbJabId'=>$jabatan[0]->jabId,
		    			'pegbTMTawal'=>$tmtawal,
		    			'pegbTMTakhir'=>$tmtakhir,
		    			'pegbUnitId'=>$jabatan[0]->unitId,
		    			'pegbGaji'=>$jabatan[0]->jabGaji,
		    			'pegbNormal'=>$jabatan[0]->jabNormal,
		    			'pegbMaksimum'=>$jabatan[0]->jabMaks,
		    			'pegbJabNama'=>$jabatan[0]->jabNama
		    		);
				} else
				{
					$total = $grs = $irs = $ire = 0;
					$grs_jab = $grs_jab_id = $grs_unitid = $grs_tmtawal = $grs_tmtakhir = '';
					$isDT = false;
					foreach($jabatan as $row)
					{	       
						if($row->jpegKet!='DS') //Tenaga Kependidikan dan Dosen Tugas Tambahan
						{
							if($total<$row->jabTotal)
							{
								$grs = $row->jabGaji;
								$irs = $row->jabNormal;
								$ire = $row->jabMaks;
								$total = $row->jabTotal;
							}

							$grs_jab = $row->jabNama;
							$grs_jab_id = $row->jabId;
							$grs_unitid = $row->unitId;
							$grs_tmtawal = $row->pegTMT;
							$grs_tmtakhir = $row->pegTMTakhir;								
						} 
						if($row->jpegKet=='DS') //Dosen Fungsional
						{
							$dsJabId = $row->jabId;
							if(empty($grs_jab))
							{
								$grs_jab = $row->jabNama;
								$grs_jab_id = $row->jabId;
								$grs_unitid = $row->unitId;
								$grs_tmtawal = $row->pegTMT;
								$grs_tmtakhir = $row->pegTMTakhir;

							} else
								$isDT = true;

							if($isDT)
							{
								$ireJabMaks = 1.5*$row->jabNormal;
								$tempTotal = $row->jabGaji+$ireJabMaks;
							} else
							{
								$ireJabMaks = $row->jabMaks;
								$tempTotal = $row->jabGaji+$ireJabMaks;
							}
							if($total<$tempTotal)
							{
								$grs = $row->jabGaji;
								$irs = $row->jabNormal;
								$ire = $ireJabMaks;
								$total = $tempTotal;

								if($isDT)
									$grs_jab .= ' ('.$row->jabNama.')';
							}

						} 
					}
					if(strtotime($grs_tmtawal) > strtotime($tmtawal))
						$tmtawal = $grs_tmtawal;
					if(strtotime($grs_tmtakhir) < strtotime($tmtakhir) and $grs_tmtakhir != '0000-00-00')
						$tmtakhir = $grs_tmtakhir;

					$param = array(
		    			'pegbNIP'=>$pegNIP,
		    			'pegbBulan'=>$bulan,
		    			'pegbTahun'=>$tahun,
		    			'pegbJabId'=>$grs_jab_id,
		    			'pegbTMTawal'=>$tmtawal,
		    			'pegbTMTakhir'=>$tmtakhir,
		    			'pegbUnitId'=>$grs_unitid,
		    			'pegbGaji'=>$grs,
		    			'pegbNormal'=>$irs,
		    			'pegbMaksimum'=>$ire,
		    			'pegbJabNama'=>$grs_jab
		    		);
				}

	    		$CI->model_master->insert('d_pegawai_history_jabatan_bulan',$param);
				//echo $pegNIP.' Berhasil Disimpan <br/>';
			}
		}

	    return true;
	}	
}