<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('pph'))
{
	function pph($param){
		if(!empty($param)) {
			$gol = explode('/',$param);
			if($gol[0]=='IV')
				$pajak = 0.15;
			elseif($gol[0]=='III')
				$pajak = 0.05;
			elseif($gol[0]=='II' or $gol[0]=='I')
				$pajak = 0;
			else
				$pajak = 999999;
			return $pajak;
		} else
			return(false);
	}
}
?>