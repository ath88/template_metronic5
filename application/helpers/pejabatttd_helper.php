
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('pejabatttd'))
{
	function pejabatttd($datas,$groupnama)
	{
	    // Get a reference to the controller object
	    $CI = get_instance();

	    // You may need to load the model if it hasn't been pre-loaded
		$CI->load->model('model_master','',TRUE);
		$CI->load->model('model_pejabatttd','',TRUE);

		$data = false;
		if(strpos($groupnama, 'BPU') !== false or strpos($groupnama, 'UPT') !== false) 
		{
			//AKSES DATA PIMPINAN UPT/BPU
		    if(strpos($groupnama, 'BPU') !== false)
		    	$row = $CI->model_pejabatttd->get_pejabat(143);
		    if(strpos($groupnama, 'UPTKEARSIPAN') !== false)
		    	$row = $CI->model_pejabatttd->get_pejabat(174);
		    if(strpos($groupnama, 'UPTPERPUSTAKAAN') !== false)
		    	$row = $CI->model_pejabatttd->get_pejabat(170);
		    
		    if($row!=false)
		    {
		    	$data = (object)array(
						'NIP'=>$row->pegbNIP,
						'NAMA'=>$row->pegNama,
						'JABATAN'=>$row->jabNama,
						'UNITNAMA'=>$datas[0]->unitNama
					);	
		    }
		} else
		{
			if($datas!=false)
			{
				$unitNama = $datas[0]->unitNama;
				if(substr($datas[0]->unitKode, 0, 2)!='01')
				{
					$row = $CI->model_master->get_by_id('r_unit',array('unitKode'=>substr($datas[0]->unitKode, 0, 2)));
					if($row!=false)
						$unitNama = $row->unitNama;
				}
				$data = (object)array(
						'NIP'=>$datas[0]->pegabsNIP,
						'NAMA'=>$datas[0]->pegNama,
						'JABATAN'=>$datas[0]->jabNama,
						'UNITNAMA'=>$unitNama
					);	
			}
			
		}

	    return $data;
	}	
}