<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('KodeUnit'))
{
	function KodeUnit($param){
		if(!empty($param)) {
			$text = $spasi = '';
			$c = 0;
			for($i=0;$i<strlen($param);$i=$i+2){
				$text .= substr($param, $i, 2).'.';
				$c++;
			}
			for($i=0;$i<$c;$i++)
				$spasi .= '&nbsp;';
			$text = $spasi.substr($text, 0, strlen($text)-1);
			return($text);
		} else
			return(false);
	}
}
?>