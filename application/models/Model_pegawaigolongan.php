<?php
class Model_pegawaigolongan extends CI_Model
{
	function get($key)
	{
		$this->db->select("*");
		$this->db->from('d_pegawai_pangkat');
		$this->db->join('d_pegawai','pegGolNomorKtp = pegNomorKtp','left');
		$this->db->join('ref_golongan','golonganKode = pegGolGolonganKode','left');
		$this->db->where('pegNomorKtp',$key);
		$this->db->where('pegIsAktif',1);
		$this->db->order_by('pegGolTmt DESC');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}

	function get_by_id($key)
	{
		$this->db->select("*");
		$this->db->from('d_pegawai_pangkat');
		$this->db->join('d_pegawai','pegGolNomorKtp = pegNomorKtp','left');
		$this->db->join('ref_golongan','golonganKode = pegGolGolonganKode','left');
		$this->db->where('pegGolId',$key);
		$this->db->where('pegIsAktif',1);
		$this->db->order_by('pegGolTmt DESC');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->row();
		else
			return false;
	}
}
?>