<?php
class Model_pegawaihistujuhbelas extends CI_Model
{
	function get($key)
	{
		$this->db->select("a.pegNIP,
							pegNama,
							pegGol,
							a.pegId,
							a.pegUnitKerja,
							a.pegKodeUnitKerja,
							d.unitNama parent1, 
							e.unitNama parent2,
							a.pegJabId,
							jabJpegId,
							jabNama,
							jabKelas,
							pegTB,
							pegSD,
							IF(pegTB=0,'TIDAK','YA') TB,
							IF(pegSD=0,'TIDAK','YA') SD,
							pegTMT,
							pegTMTakhir,
							b.pegNoRek,
							pegValidate");
		$this->db->from('d_pegawai_history_jabatan_2017 a');
		$this->db->join('d_pegawai b','a.pegNIP = b.pegNIP','left');
		$this->db->join('r_unit c','a.pegKodeUnitKerja = c.unitId','LEFT');
		$this->db->join('r_unit d','LEFT(c.unitKode,4) = d.unitKode','LEFT');
		$this->db->join('r_unit e','LEFT(c.unitKode,2) = e.unitKode','LEFT');
		$this->db->join('r_jabatan_2017','pegJabId = jabId','left');
		$this->db->where('a.pegNIP',$key);
		$this->db->where('b.pegIsPensiun',0);
		$this->db->order_by('pegTMT DESC');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}

	function get_by_id($key)
	{
		$this->db->select("
							a.pegId,
							a.pegNIP,
							pegNama,
							pegGol,
							a.pegId,
							a.pegUnitKerja,
							a.pegKodeUnitKerja,
							a.pegJabId,
							jabJpegId,
							jabNama,
							jabKelas,
							pegTB,
							pegSD,
							IF(pegTB=0,'TIDAK','YA') TB,
							IF(pegSD=0,'TIDAK','YA') SD,
							pegTMT,
							pegTMTakhir,
							b.pegNoRek,
							pegValidate");
		$this->db->from('d_pegawai_history_jabatan_2017 a');
		$this->db->join('d_pegawai b','a.pegNIP = b.pegNIP','left');
		$this->db->join('r_jabatan_2017','pegJabId = jabId','left');
		$this->db->where('a.pegId',$key);
		$this->db->order_by('pegTMT DESC');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->row();
		else
			return false;
	}
}
?>