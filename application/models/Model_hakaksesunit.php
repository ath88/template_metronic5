<?php
class Model_hakaksesunit extends CI_Model
{
	function get($sgroupNama)
	{
		$this->db->select("a.unitId,a.unitKode,CONCAT(a.unitNama,' (',temp.unitNama,')') unitNama,sgroupunitUnitId");
		$this->db->from('s_user_group_unit');
		$this->db->join('s_unit a',"sgroupunitUnitId=a.unitId",'left');
		$this->db->join('(SELECT unitKode,unitNama FROM s_unit WHERE LENGTH(unitKode) = 2) temp',"LEFT(a.unitKode,2)=temp.unitKode",'left');
		$this->db->where('sgroupunitSgroupNama',$sgroupNama);
		$this->db->group_by('a.unitId');
		$this->db->order_by('unitKode');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}	
}
?>