<?php
class Model_login extends CI_Model
{
	function get_user_login($username, $password)
	{
		$this->db->select('susrNama,susrPassword,susrSgroupNama,susrProfil');
		$this->db->from('s_user');
		$this->db->where('susrNama', $username);
		$this->db->where('susrPassword',MD5($password));
		$this->db->limit(1);

		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return false;
	}

	function get_user($username)
	{
		$this->db->select('susrNama,susrSgroupNama,susrProfil');
		$this->db->from('s_user');
		$this->db->where('susrNama', $username);
		$this->db->limit(1);

		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return false;
	}

	function ubah($table,$key,$param)
	{
		$this->db->trans_start();
		$this->db->where($key['id'], $key['value']);
		$this->db->update($table, $param); 
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}
}
?>