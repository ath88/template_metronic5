<?php
class Model_pegawaijabatan extends CI_Model
{
	function get($key)
	{
		$this->db->select("*");
		$this->db->from('d_pegawai_jabatan');
		$this->db->join('d_pegawai','pegJabNomorKtp = pegNomorKtp','left');
		$this->db->join('ref_jabatan','jabatanId = pegJabJabatanId','left');
		$this->db->where('pegNomorKtp',$key);
		$this->db->where('pegIsAktif',1);
		$this->db->order_by('pegJabTMT DESC');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}

	function get_by_id($key)
	{
		$this->db->select("*");
		$this->db->from('d_pegawai_jabatan');
		$this->db->join('d_pegawai','pegJabNomorKtp = pegNomorKtp','left');
		$this->db->join('ref_jabatan','jabatanId = pegJabJabatanId','left');
		$this->db->where('pegJabId',$key);
		$this->db->where('pegIsAktif',1);
		$this->db->order_by('pegJabTMT DESC');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->row();
		else
			return false;
	}
}
?>