<?php
class Model_hakaksesmodul extends CI_Model
{
	function get($sgroupNama)
	{
		$this->db->select('susrmodulNama,susrmodulNamaDisplay,sgroupmodulSgroupNama,sgroupmodulSusrmodulNama,susrmdgroupDisplay');
		$this->db->from('s_user_modul_ref');
		$this->db->join('s_user_group_modul',"susrmodulNama=sgroupmodulSusrmodulNama and sgroupmodulSgroupNama='$sgroupNama'",'left');
		$this->db->join('s_user_modul_group_ref',"susrmdgroupNama=susrmodulSusrmdgroupNama",'left');
		$this->db->where('susrmodulIsLogin','1');
		$this->db->order_by('susrmodulSusrmdgroupNama,susrmodulNama');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}	
}
?>