<?php
class Model_pengguna extends CI_Model
{
	function get()
	{
		$this->db->select("*");
		$this->db->from('s_user');
		$this->db->join('s_user_group',"susrSgroupNama=sgroupNama",'left');
		$this->db->where("sgroupNama <> 'DOSEN'",false,false);
		$this->db->where("sgroupNama <> 'STRUKTURAL'",false,false);
		$this->db->where("sgroupNama <> 'DOSEN_STRUKTURAL'",false,false);
		$this->db->order_by('sgroupNama,susrNama');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}

	function get_by_id($susrNama)
	{
		$this->db->select("*");
		$this->db->from('s_user');
		$this->db->join('s_user_group',"susrSgroupNama=sgroupNama",'left');
		$this->db->where('susrNama',$susrNama);
		$this->db->order_by('sgroupNama,susrNama');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->row();
		else
			return false;
	}
}
?>