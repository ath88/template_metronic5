<?php
class Model_pegawai extends CI_Model
{
	function get_by_unitid($key)
	{
		$this->db->select("pegJabNomorKtp,pegJabJabatanId,MAX(pegJabTMT) jabTMT",FALSE);
		$this->db->from('d_pegawai_jabatan');
		$this->db->group_by('pegJabNomorKtp');
		$subquery = $this->db->_compile_select();
		$this->db->_reset_select();

		$this->db->select("pegJabNomorKtp,pegJabJabatanId,jabatanUraian",FALSE);
		$this->db->from('('.$subquery.') as datas');
		$this->db->join('ref_jabatan','pegJabJabatanId=jabatanId','left');
		$this->db->order_by('pegJabNomorKtp');
		$subsubquery = $this->db->_compile_select();
		$this->db->_reset_select();

		$this->db->select("pegGolNomorKtp,pegGolGolonganKode,MAX(pegGolTmt) jabTMT",FALSE);
		$this->db->from('d_pegawai_pangkat');
		$this->db->group_by('pegGolNomorKtp');
		$subquery2 = $this->db->_compile_select();
		$this->db->_reset_select();

		$this->db->select("pegGolNomorKtp,pegGolGolonganKode,golonganUraian",FALSE);
		$this->db->from('('.$subquery2.') as datas2');
		$this->db->join('ref_golongan','pegGolGolonganKode=golonganKode','left');
		$this->db->order_by('pegGolNomorKtp');
		$subsubquery2 = $this->db->_compile_select();
		$this->db->_reset_select();

		$this->db->select("*");
		$this->db->from('d_pegawai');
		$this->db->join('('.$subsubquery.') as datas3','pegNomorKtp=pegJabNomorKtp','left');
		$this->db->join('('.$subsubquery2.') as datas4','pegNomorKtp=pegGolNomorKtp','left');
		$this->db->join('s_unit','unitId=pegUnitId','left');
		$this->db->where('pegUnitId',$key);
		$this->db->order_by('pegNip DESC,pegNama');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}

	function get_by_nomorktp($key)
	{
		$this->db->select("pegJabNomorKtp,pegJabJabatanId,MAX(pegJabTMT) jabTMT",FALSE);
		$this->db->from('d_pegawai_jabatan');
		$this->db->group_by('pegJabNomorKtp');
		$subquery = $this->db->_compile_select();
		$this->db->_reset_select();

		$this->db->select("pegJabNomorKtp,pegJabJabatanId,jabatanUraian",FALSE);
		$this->db->from('('.$subquery.') as datas');
		$this->db->join('ref_jabatan','pegJabJabatanId=jabatanId','left');
		$this->db->order_by('pegJabNomorKtp');
		$subsubquery = $this->db->_compile_select();
		$this->db->_reset_select();

		$this->db->select("pegGolNomorKtp,pegGolGolonganKode,MAX(pegGolTmt) jabTMT",FALSE);
		$this->db->from('d_pegawai_pangkat');
		$this->db->group_by('pegGolNomorKtp');
		$subquery2 = $this->db->_compile_select();
		$this->db->_reset_select();

		$this->db->select("pegGolNomorKtp,pegGolGolonganKode,golonganUraian",FALSE);
		$this->db->from('('.$subquery2.') as datas2');
		$this->db->join('ref_golongan','pegGolGolonganKode=golonganKode','left');
		$this->db->order_by('pegGolNomorKtp');
		$subsubquery2 = $this->db->_compile_select();
		$this->db->_reset_select();

		$this->db->select("*");
		$this->db->from('d_pegawai');
		$this->db->join('('.$subsubquery.') as datas3','pegNomorKtp=pegJabNomorKtp','left');
		$this->db->join('('.$subsubquery2.') as datas4','pegNomorKtp=pegGolNomorKtp','left');
		$this->db->join('s_unit','unitId=pegUnitId','left');
		$this->db->where('pegNomorKtp',$key);
		$this->db->order_by('pegNip DESC,pegNama');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->row();
		else
			return false;
	}

	function get_by_keyword($key,$hakakses)
	{
		$this->db->select("DISTINCT b.*, d.unitNama parent1, e.unitNama parent2",FALSE);
		$this->db->from('d_pegawai_history_jabatan_2017 a');
		$this->db->join('d_pegawai b','a.pegNIP = b.pegNIP','LEFT');
		$this->db->join('r_unit c','b.pegKodeUnitKerja = c.unitId','LEFT');
		$this->db->join('r_unit d','LEFT(c.unitKode,4) = d.unitKode','LEFT');
		$this->db->join('r_unit e','LEFT(c.unitKode,2) = e.unitKode','LEFT');
		$this->db->join('s_user_group_unit','a.pegKodeUnitKerja = sgroupunitUnitId','LEFT');
		$this->db->where('sgroupunitSgroupNama',$hakakses);
		$this->db->where("(a.pegNIP LIKE '%$key%' OR pegNama LIKE '%$key%')");
		$this->db->order_by('pegGol DESC,pegNama');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
}
?>