<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class pegawai extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/pegawai/";
	private $_path_js = "pegawai/";
	private $_page_index = 'index';
	private $_page_form = 'form';
	private $_controller_name = 'pegawai';
	private $_judul = 'Master Pegawai';

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_pegawai','',TRUE);

		if($this->session->userdata('logged_in')==false) //cek user logged
			redirect('login','refresh');
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_master->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}			

		$unitarray = $this->model_master->get_unit($session_data['susrSgroupNama']);	

		$data['listpegawai'] = false;			
		$data['pegawai'] = false;

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['judul'] = $this->_judul;
		
		$data['unit'] = $unitarray;

		return $data;
	}

	public function index()
	{	
		$data = $this->get_master($this->_path_page.$this->_page_index);
		$data['scripts'] = array($this->_path_js.$this->_controller_name);
		$data['response_url'] = site_url($this->_controller_name.'/response');
		$this->load->view($this->template, $data);
	}

	public function response()
	{		
		$session_data = $this->session->userdata('logged_in');

		$this->form_validation->set_rules('unitId','unitId','trim|required|xss_clean');

		if($this->form_validation->run()) 
		{
			if(IS_AJAX)
            {
				$unitId = $this->input->post('unitId');

				$data['datas'] = $this->model_pegawai->get_by_unitid($unitId);
				$key = $this->encryptions->encode($unitId,$this->config->item('encryption_key'));;

				$data['judul'] = $this->_judul;
				$data['add_url'] = site_url($this->_controller_name.'/tambah').'/'.$key;
				$data['edit_url'] = site_url($this->_controller_name.'/ubah').'/';
				$data['delete_url'] = site_url($this->_controller_name.'/hapus').'/';

				$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
				$data['susrNama'] = $session_data['susrNama'];

				$pages = $this->_path_page.'response';
                $this->load->view($pages,$data);
            }			
		}
		else 
           message('Ooops!! Something Wrong!!','error');
	}

	public function tambah()
	{		
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));

		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$data['agama'] = $this->model_master->get_ref_table('ref_agama');
		$data['statusnikah'] = $this->model_master->get_ref_table('ref_status_nikah');
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';	
		$data['status_page'] = 'Tambah';
		$data['datas'] = false;	
		$this->load->view($this->template, $data);
	}
	
	public function ubah()
	{		
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));

		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$data['agama'] = $this->model_master->get_ref_table('ref_agama');
		$data['statusnikah'] = $this->model_master->get_ref_table('ref_status_nikah');
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';	
		$data['status_page'] = 'Ubah';
		$data['datas'] = $this->model_pegawai->get_by_nomorktp($id);
		$this->load->view($this->template, $data);
	}
	
	public function simpan()
	{		
		$this->form_validation->set_rules('pegNip','pegNip','trim|required|xss_clean');
		$this->form_validation->set_rules('pegNomorKtp','pegNomorKtp','trim|xss_clean');
		$this->form_validation->set_rules('pegNama','pegNama','trim|required|xss_clean');
		$this->form_validation->set_rules('pegGelarDepan','pegGelarDepan','trim|xss_clean');
		$this->form_validation->set_rules('pegGelarBelakang','pegGelarBelakang','trim|xss_clean');
		$this->form_validation->set_rules('pegKotaLahir','pegKotaLahir','trim|required|xss_clean');
		$this->form_validation->set_rules('pegTanggalLahir','pegTanggalLahir','trim|required|xss_clean');
		$this->form_validation->set_rules('pegJenisKelamin','pegJenisKelamin','trim|required|xss_clean');
		$this->form_validation->set_rules('pegGolonganDarah','pegGolonganDarah','trim|required|xss_clean');
		$this->form_validation->set_rules('pegAgmrId','pegAgmrId','trim|required|xss_clean');
		$this->form_validation->set_rules('pegStnkrId','pegStnkrId','trim|required|xss_clean');
		$this->form_validation->set_rules('pegAlamat','pegAlamat','trim|required|xss_clean');
		$this->form_validation->set_rules('pegNoHP','pegNoHP','trim|required|xss_clean');
		$this->form_validation->set_rules('pegEmail','pegEmail','trim|required|xss_clean');
		$this->form_validation->set_rules('pegUnitId','pegUnitId','trim|required|xss_clean');
		$this->form_validation->set_rules('pegIsAktif','pegIsAktif','trim|required|xss_clean');

		if($this->form_validation->run()) 
		{	
			if(IS_AJAX)
	        {
	        	$pegNomorKtpOld = $this->input->post('pegNomorKtpOld');
	        	$pegNip = str_replace(' ', '', $this->input->post('pegNip'));
	        	$pegNomorKtp = str_replace(' ', '', $this->input->post('pegNomorKtp'));
	        	$pegNama = $this->input->post('pegNama');  	
	        	$pegGelarDepan = $this->input->post('pegGelarDepan'); 
	        	$pegGelarBelakang = $this->input->post('pegGelarBelakang'); 
	        	$pegKotaLahir = $this->input->post('pegKotaLahir'); 
	        	$pegTanggalLahir = $this->input->post('pegTanggalLahir'); 
	        	$pegJenisKelamin = $this->input->post('pegJenisKelamin'); 
	        	$pegGolonganDarah = $this->input->post('pegGolonganDarah'); 
	        	$pegAgmrId = $this->input->post('pegAgmrId'); 
	        	$pegStnkrId = $this->input->post('pegStnkrId'); 
	        	$pegAlamat = $this->input->post('pegAlamat'); 
	        	$pegNoHP = $this->input->post('pegNoHP'); 
	        	$pegEmail = $this->input->post('pegEmail'); 
	        	$pegUnitId = $this->input->post('pegUnitId'); 
	        	$pegIsAktif = $this->input->post('pegIsAktif'); 

        		$param = array(
        			'pegNip'=>$pegNip,
        			'pegNomorKtp'=>$pegNomorKtp,
        			'pegNama'=>$pegNama,
        			'pegGelarDepan'=>$pegGelarDepan,
        			'pegGelarBelakang'=>$pegGelarBelakang,
        			'pegKotaLahir'=>$pegKotaLahir,
        			'pegTanggalLahir'=>$pegTanggalLahir,
        			'pegJenisKelamin'=>$pegJenisKelamin,
        			'pegGolonganDarah'=>$pegGolonganDarah,
        			'pegAgmrId'=>$pegAgmrId,
        			'pegStnkrId'=>$pegStnkrId,
        			'pegAlamat'=>$pegAlamat,
        			'pegNoHP'=>$pegNoHP,
        			'pegEmail'=>$pegEmail,
        			'pegUnitId'=>$pegUnitId,
        			'pegIsAktif'=>$pegIsAktif	
        		);

	        	if(empty($pegNomorKtpOld))
	        	{
	        		$proses = $this->model_master->insert('d_pegawai',$param);
	        	} else {
	        		$key = array('pegNomorKtp'=>$pegNomorKtpOld);
	        		$proses = $this->model_master->update('d_pegawai',$param,$key);
	        	}

	        	if($proses)
	        	{	        
	        		message($this->_judul.' Berhasil Disimpan','success');
	        	}
	        	else
	        		message($this->_judul.' Gagal Disimpan','error');
	        }
	    } else {
	    	message('Ooops!! Something Wrong!!','error');
	    }
	}
	
	public function hapus()
	{
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$proses = $this->model_master->delete('d_pegawai',array('pegNomorKtp'=>$id));
		if ($proses) 
			message($this->_judul.' Berhasil Dihapus','success');
		else
			message($this->_judul.' Gagal Dihapus','error');
	}
	
}
