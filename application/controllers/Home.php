<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {
	var $template = 'template';

	private $_path_page = "pages/home/";
	private $_path_js = "home/";
	private $_judul = 'Dashboard';
	private $_page_index = 'index';	
	private $_controller_name = 'home';

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_master','',TRUE);

		if($this->session->userdata('logged_in')==false) //cek user logged
			redirect('login','refresh');
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_master->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database											
		//echo $this->db->last_query();

		$data['breadcrumb'] = (object)array('susrmdgroupDisplay'=>'Dashboard','susrmodulNamaDisplay'=>'Dashboard');
		$data['page'] = $pages;
		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];

		$data['menu'] = $menu;
		$data['judul'] = $this->_judul;

		return $data;
	}

	public function index()
	{
		$data = $this->get_master($this->_path_page.$this->_page_index);
		//$tahun = $this->model_master->get_by_id('r_tahun',array('tahunIsAktif'=>1));
		//$data['datas'] = $this->model_master->get_rekap_pegawai($data['susrSgroupNama'],$tahun->tahun);
		$data['datas'] = false;
		$data['scripts'] = array($this->_path_js.'profile');
		$this->load->view($this->template, $data);
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('home','refresh');
	}

	function ubahpass()
	{
		$session_data = $this->session->userdata('logged_in');
		$data = $this->get_master($this->_path_page.'ubahpassword');	
		$data['save_url'] = site_url($this->_controller_name.'/prosesubahpassword');		
		$data['judul'] = 'Ubah Password';
		$data['scripts'] = array($this->_path_js."ubahpassword");		
		$this->load->view($this->template, $data);

	}

	function ubahhakakses()
	{
		$session_data = $this->session->userdata('logged_in');
		if($session_data['susrSgroupNama_ori']=='ADMIN')
		{
			$data = $this->get_master($this->_path_page.'ubahhakakses');
			$data['save_url'] = site_url($this->_controller_name.'/prosesubahhakakses');		
			$data['judul'] = 'Ubah Hak Akses';
			$data['hakakses'] = $this->model_master->get_ref_table('s_user_group');
			$data['scripts'] = array($this->_path_js."ubahhakakses");		
			$this->load->view($this->template, $data);
		} else
			redirect('login','refresh');
	}

	function prosesubahpassword()
	{
		$session_data = $this->session->userdata('logged_in');

		$this->form_validation->set_rules('susrPasswordOld','susrPasswordOld','trim|required|xss_clean');
		$this->form_validation->set_rules('susrPasswordNew','susrPasswordNew','trim|required|xss_clean');
		$this->form_validation->set_rules('susrPasswordNewConfirm','susrPasswordNewConfirm','trim|required|xss_clean');

		if($this->form_validation->run())  
		{

			$susrPasswordOld = md5($this->input->post('susrPasswordOld'));
			$susrPasswordNew = md5($this->input->post('susrPasswordNew'));
			$susrPasswordNewConfirm = md5($this->input->post('susrPasswordNewConfirm'));

			$susrNama = $session_data['susrNama'];
			$susrSgroupNama = $session_data['susrSgroupNama'];
			$susrProfil = $session_data['susrProfil'];

			$key = array('susrNama'=>$susrNama,'susrPassword'=>$susrPasswordOld);
			$cek_user = $this->model_master->get_by_id('s_user',$key);

			if($cek_user!=false)
			{
				if($susrPasswordNew==$susrPasswordNewConfirm)
				{
					$param = array('susrPassword'=>$susrPasswordNew);			
					$ubahPass = $this->model_master->update('s_user',$param,$key);
					if ($ubahPass) 
						echo message('Password Berhasil Diubah','success');
					else
						echo message('Password Gagal diubah','error');
				} else
					echo message('Password Baru Tidak Sama Dengan Konfirmasi','error');
			}
			else
				echo message('Username dan Password Lama Salah','error');
		} else
			message('Ooops!! Something Wrong!!','error');
	}

	function prosesubahhakakses()
	{
		$session_data = $this->session->userdata('logged_in');

		$this->form_validation->set_rules('HAKAKSES','HAKAKSES','trim|required|xss_clean');

		if($this->form_validation->run())  
		{
			if($session_data['susrSgroupNama_ori']=='ADMIN')
			{
				$hakakses_new = $this->input->post('HAKAKSES');
				$sess_array = array(
					'susrNama' => $session_data['susrNama'],
					'susrSgroupNama' => $hakakses_new,
					'susrSgroupNama_ori' => $session_data['susrSgroupNama_ori'],
					'susrProfil' => $session_data['susrProfil']
				);
				$this->session->set_userdata('logged_in',$sess_array);
				echo message('Hak Akses Berhasil Diubah','success');
			} else
				redirect('login','refresh');
		} else
			message('Ooops!! Something Wrong!!','error');
	}
}
