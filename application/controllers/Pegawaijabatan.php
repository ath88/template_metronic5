<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class pegawaijabatan extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/pegawaijabatan/";
	private $_path_js = "pegawai/";
	private $_page_index = 'index';
	private $_page_form = 'form';
	private $_controller_name = 'pegawaijabatan';
	private $_judul = 'History Jabatan';

	private $_user_grant = array('admin','sonny','gafur','ig','an','bejo','hr','ocean','soepriyadi','mika','tego');

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_pegawaijabatan','',TRUE);
		$this->load->helper('pegawaihisjabbulan');

		if($this->session->userdata('logged_in')==false) //cek user logged
			redirect('login','refresh');
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_master->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}			

		$unitarray = $this->model_master->get_unit($session_data['susrSgroupNama']);	

		$data['listpegawai'] = false;			
		$data['pegawai'] = false;

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['judul'] = $this->_judul;
		
		$data['unit'] = $unitarray;

		return $data;
	}

	public function loadpegawai()
	{		
		$session_data = $this->session->userdata('logged_in');

		$this->form_validation->set_rules('unitid','unitid','trim|required|xss_clean');

		if($this->form_validation->run()) 
		{
			if(IS_AJAX)
            {
				$unitId = $this->input->post('unitid');

				$data = $this->model_master->get_ref_table('d_pegawai','pegNIP',array('pegUnitId'=>$unitId));	

				if($data!=false) {
					echo '<option value=""></option>';
					foreach($data as $row)
					{
						echo '<option value="'.$row->pegNomorKtp.'">'.((!empty($row->pegNip) and $row->pegNip!='-')?$row->pegNip:$row->pegNomorKtp).' - '.(!empty($row->pegGelarDepan)?$row->pegGelarDepan.'. ':'').$row->pegNama.(!empty($row->pegGelarBelakang)?', '.$row->pegGelarBelakang:'').'</option>';
					}
				}
            }			
		}
		else 
           message('Ooops!! Something Wrong!!','error');
	}

	public function index()
	{	
		$data = $this->get_master($this->_path_page.$this->_page_index);
		$data['scripts'] = array($this->_path_js.$this->_controller_name,'load_pegawai');
		$data['response_url'] = site_url($this->_controller_name.'/response');
		$this->load->view($this->template, $data);
	}

	public function response()
	{		
		$session_data = $this->session->userdata('logged_in');
		$this->form_validation->set_rules('unitId','unitId','trim|required|xss_clean');
		$this->form_validation->set_rules('pegNomorKtp','pegNomorKtp','trim|required|xss_clean');

		if($this->form_validation->run()) 
		{
			if(IS_AJAX)
            {
				$pegNomorKtp = $this->input->post('pegNomorKtp');

				$data['datas'] = $this->model_pegawaijabatan->get($pegNomorKtp);
				$key = $this->encryptions->encode($pegNomorKtp,$this->config->item('encryption_key'));;

				$data['judul'] = $this->_judul;
				$data['add_url'] = site_url('pegawaijabatan/tambah').'/'.$key;
				$data['edit_url'] = site_url('pegawaijabatan/ubah').'/';
				$data['delete_url'] = site_url('pegawaijabatan/hapus').'/';

				$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
				$data['susrNama'] = $session_data['susrNama'];

				$pages = $this->_path_page.'response';
                $this->load->view($pages,$data);
            }				
		}
		else 
			message('Ooops!! Something Wrong!!','error');
	}

	public function tambah()
	{		
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));

		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';	
		$data['status_page'] = 'Tambah';
		$data['pegawai'] = $this->model_master->get_by_id('d_pegawai',array('pegNomorKtp'=>$id));
		$data['jabatan'] = $this->model_master->get_ref_table('ref_jabatan');
		$data['datas'] = false;	
		$this->load->view($this->template, $data);
	}	
	
	public function ubah()
	{		
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));

		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';	
		$data['status_page'] = 'Ubah';
		$data['datas'] = $this->model_pegawaijabatan->get_by_id($id);		
		$data['pegawai'] = $this->model_master->get_by_id('d_pegawai',array('pegNomorKtp'=>$data['datas']->pegJabNomorKtp));
		$data['jabatan'] = $this->model_master->get_ref_table('ref_jabatan');	
		$this->load->view($this->template, $data);
	}	
	
	public function simpan()
	{		
		$session_data = $this->session->userdata('logged_in');

		$this->form_validation->set_rules('pegJabNomorKtp','pegJabNomorKtp','trim|required|xss_clean');
		$this->form_validation->set_rules('pegJabJabatanId','pegJabJabatanId','trim|required|xss_clean');
		$this->form_validation->set_rules('pegJabNoSK','pegJabNoSK','trim|required|xss_clean');
		$this->form_validation->set_rules('pegJabTanggalSK','pegJabTanggalSK','trim|required|xss_clean');
		$this->form_validation->set_rules('pegJabTMT','pegJabTMT','trim|required|xss_clean');

		if($this->form_validation->run())  
		{

			$pegJabId = $this->input->post('pegJabId');
			$pegJabNomorKtp = $this->input->post('pegJabNomorKtp');
			$pegJabJabatanId = $this->input->post('pegJabJabatanId');
			$pegJabNoSK = $this->input->post('pegJabNoSK');
			$pegJabTanggalSK = $this->input->post('pegJabTanggalSK');	
			$pegJabTMT = $this->input->post('pegJabTMT');		

			$param = array(
				'pegJabNomorKtp'=>$pegJabNomorKtp,
				'pegJabJabatanId'=>$pegJabJabatanId,
				'pegJabNoSK'=>$pegJabNoSK,
				'pegJabTanggalSK'=>$pegJabTanggalSK,
				'pegJabTMT'=>$pegJabTMT
			);

			$path = './assets/upload_file/'.$pegJabNomorKtp.'/';

	        if(!file_exists($path))
    			mkdir($path, 0755);

    		$config['file_name']          	= $pegJabTanggalSK.'_'.$pegJabJabatanId.'_1';
			$config['upload_path']          = $path;
            $config['allowed_types']        = 'pdf';
            $config['overwrite']        	= TRUE;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('customFile') and !empty($_FILES['customFile']['name']))
            {
            	$prosesupload = false;
                message('Gagal: '.$this->upload->display_errors(),'error');
            }
            else
            {
            	if(empty($pegJabId)) 
				{
					$proses = $this->model_master->insert('d_pegawai_jabatan',$param);
				} else {
					
					$key = array('pegJabId'=>$pegJabId);
		        	$proses = $this->model_master->update('d_pegawai_jabatan',$param,$key);
				}

				if ($proses) 
					message($this->_judul.' Berhasil Disimpan','success');
				else
					message($this->_judul.' Gagal Disimpan','error');
            }			
		} else
			message('Ooops!! Something Wrong!!','error');
	}	
	
	public function hapus($id = '')
	{
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$datas = $this->model_pegawaijabatan->get_by_id($id);	

		$proses = $this->model_master->delete('d_pegawai_jabatan',array('pegJabId'=>$id));
		if ($proses) 
		{
			$filename = './assets/upload_file/'.$datas->pegJabNomorKtp.'/'.$datas->pegJabTanggalSK.'_'.$datas->pegJabJabatanId.'_1.pdf';
			if(file_exists($filename))
				unlink($filename);
			message($this->_judul.' Berhasil Dihapus','success');
		}
		else
			message($this->_judul.' Gagal Dihapus','error');
	}
}
