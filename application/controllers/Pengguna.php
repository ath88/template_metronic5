<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class pengguna extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/pengguna/";
	private $_path_js = "administrator/";
	private $_page_index = 'index';
	private $_page_form = 'form';
	private $_controller_name = 'pengguna';
	private $_judul = 'Pengguna';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_pengguna','',TRUE);

		if($this->session->userdata('logged_in')==false) //cek user logged
			redirect('login','refresh');
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_master->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database			

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	
		$pengguna = $this->model_pengguna->get();
		

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}				

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['pengguna'] = $pengguna;
		$data['judul'] = $this->_judul;

		return $data;
	}

	public function index()
	{		
		$data = $this->get_master($this->_path_page.$this->_page_index);
		$data['scripts'] = array($this->_path_js.$this->_controller_name);
		$data['add_url'] = site_url($this->_controller_name.'/tambah').'/';
		$data['edit_url'] = site_url($this->_controller_name.'/ubah').'/';
		$data['delete_url'] = site_url($this->_controller_name.'/hapus').'/';
		$data['resetpass_url'] = site_url($this->_controller_name.'/resetpassword').'/';
		$this->load->view($this->template, $data);
	}

	public function tambah()
	{	
		$this->load->helper("generate");
		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$data['hakakses'] = $this->model_master->get_ref_table('s_user_group');
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';
		$data['password']=generatePassword();	
		$data['status_page'] = 'Tambah';
		$data['datas'] = false;	
		$this->load->view($this->template, $data);
	}

	public function ubah()
	{		
		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$data['hakakses'] = $this->model_master->get_ref_table('s_user_group');
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';
		$data['password']='';	
		$data['status_page'] = 'Ubah';
		$data['datas'] = $this->model_pengguna->get_by_id($keyS);	
		$this->load->view($this->template, $data);
	}

	public function simpan()
	{		
		$this->form_validation->set_rules('susrNama','susrNama','trim|required|xss_clean');
		$this->form_validation->set_rules('susrProfil','susrProfil','trim|required|xss_clean');
		$this->form_validation->set_rules('susrSgroupNama','susrSgroupNama','trim|required|xss_clean');
		$this->form_validation->set_rules('susrPassword','susrPassword','trim|xss_clean');
		if($this->form_validation->run()) 
		{	
			if(IS_AJAX)
	        {
	        	$susrNamaOld = $this->input->post('susrNamaOld');
	        	$susrNama = $this->input->post('susrNama');
	        	$susrProfil = $this->input->post('susrProfil');
	        	$susrSgroupNama = $this->input->post('susrSgroupNama');  	
	        	$susrPassword = $this->input->post('susrPassword'); 

        		$param = array(
        			'susrNama'=>$susrNama,
        			'susrProfil'=>$susrProfil,
        			'susrSgroupNama'=>$susrSgroupNama
        		);

	        	if(empty($susrNamaOld))
	        	{
	        		$param['susrPassword'] = $susrPassword;
	        		$proses = $this->model_master->insert('s_user',$param);
	        	} else {
	        		$key = array('susrNama'=>$susrNamaOld);
	        		$proses = $this->model_master->update('s_user',$param,$key);
	        	}

	        	if($proses)
	        		message($this->_judul.' Berhasil Disimpan','success');
	        	else
	        		message($this->_judul.' Gagal Disimpan','error');
	        }
	    } else {
	    	message('Ooops!! Something Wrong!!','error');
	    }
	}	
	
	public function hapus()
	{
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$proses = $this->model_master->delete('s_user',array('susrNama'=>$id));
		if ($proses) 
			message($this->_judul.' Berhasil Dihapus','success');
		else
			message($this->_judul.' Gagal Dihapus','error');
	}	
	
	public function resetpassword()
	{		
		$session_data = $this->session->userdata('logged_in');
		if(IS_AJAX)
        {
			$this->load->helper("generate");
        	$key = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			$password=generatePassword();

			$param = array(
        			'susrPassword'=>md5($password)
        		);
			$keys = array('susrNama'=>$key);
	        $proses = $this->model_master->update('s_user',$param,$keys);

			if ($proses) 
				message('Password '.$this->_judul.' Berhasil Diubah!! Username: <strong>'.$key.'</strong>, Password: <strong>'.$password.'</strong>','success');
			else
				message('Password '.$this->_judul.' Gagal Diubah!!','error');
        }	
	}
}
