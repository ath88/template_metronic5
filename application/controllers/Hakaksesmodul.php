<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class hakaksesmodul extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/hakaksesmodul/";
	private $_path_js = "administrator/";
	private $_page_index = 'index';
	private $_page_form = 'form';
	private $_controller_name = 'hakaksesmodul';
	private $_judul = 'Hak Akses Modul';


	function __construct()
	{
		parent::__construct();
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_hakaksesmodul','',TRUE);

		if($this->session->userdata('logged_in')==false) //cek user logged
			redirect('login','refresh');
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_master->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database			

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	
		$hakakses = $this->model_master->get_ref_table('s_user_group');

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}				

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['hakakses'] = $hakakses;
		$data['judul'] = $this->_judul;
		
		return $data;
	}

	public function index()
	{		
		$data = $this->get_master($this->_path_page.$this->_page_index);
		$data['scripts'] = array($this->_path_js.$this->_controller_name);
		$data['response_url'] = site_url($this->_controller_name.'/response');
		$this->load->view($this->template, $data);
	}
	
	public function response()
	{		
		$this->form_validation->set_rules('hakakses','hakakses','trim|required|xss_clean');

		if($this->form_validation->run()) 
		{	
			if(IS_AJAX)
	        {
	        	$hakakses = $this->input->post('hakakses');
	        	$datas = $this->model_hakaksesmodul->get($hakakses);

	        	$data['datas'] = $datas;
	        	$data['sgroupNama'] = $hakakses;
	        	$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';
	        	$data['judul'] = $this->_judul;
				$pages = $this->_path_page.'response';
                $this->load->view($pages,$data);
	        }
	    } else {
	    	message('Ooops!! Something Wrong!!','error');
	    }
	}
			
	public function simpan()
	{
		$this->form_validation->set_rules('cekModul[]','cekModul','trim|required|xss_clean');
		$this->form_validation->set_rules('sgroupNama','sgroupNama','trim|required|xss_clean');
		if($this->form_validation->run()) 
		{	
			if(IS_AJAX)
	        {
	        	$cekModul = $this->input->post('cekModul');	
	        	$sgroupNama = $this->input->post('sgroupNama');	

	        	if(count($cekModul)>0)
		        {
		        	$this->model_master->delete('s_user_group_modul',array('sgroupmodulSgroupNama'=>$sgroupNama));

		        	foreach ($cekModul as $cek => $modul) {		
		        		$param = array(
		        			'sgroupmodulSgroupNama'=>$sgroupNama,
		        			'sgroupmodulSusrmodulNama'=>$modul,
		        			'sgroupmodulSusrmodulRead'=>1,
		        		);	 
						$this->model_master->insert('s_user_group_modul',$param);
				  	}

	        		message($this->_judul.' Berhasil Disimpan','success');
	        	} else
	        		message('Pilih Menu!! Minimal 1','error');
	        }
	    } else {
	    	message('Ooops!! Something Wrong!!','error');
	    }		
	}	
}
