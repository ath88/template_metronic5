<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class pegawaihistujuhbelas extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/pegawaihistujuhbelas/";
	private $_path_js = "pegawai/";
	private $_page_index = 'index';
	private $_page_form = 'form';
	private $_controller_name = 'pegawaihistujuhbelas';
	private $_judul = 'Pegawai History';

	private $_user_grant = array('admin','sonny','gafur','ig','an','bejo','hr','ocean','soepriyadi','mika','tego');

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_pegawaihistujuhbelas','',TRUE);
		$this->load->helper('pegawaihisjabbulan');

		if($this->session->userdata('logged_in')==false) //cek user logged
			redirect('login','refresh');
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_master->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}			

		$unitarray = $this->model_master->get_unit($session_data['susrSgroupNama']);	

		$data['listpegawai'] = false;			
		$data['pegawai'] = false;

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['judul'] = $this->_judul;
		
		$data['unit'] = $unitarray;

		return $data;
	}

	public function loadpegawai()
	{		
		$session_data = $this->session->userdata('logged_in');

		$this->form_validation->set_rules('unitid','unitid','trim|required|xss_clean');

		if($this->form_validation->run()) 
		{
			if(IS_AJAX)
            {
				$unitId = $this->input->post('unitid');

				$data = $this->model_master->get_ref_table('d_pegawai','pegNIP',array('pegKodeUnitKerja'=>$unitId));	

				if($data!=false) {
					echo '<option value=""></option>';
					foreach($data as $row)
					{
						echo '<option value="'.$row->pegNIP.'">'.strtoupper($row->pegUnitKerja).' - '.$row->pegNIP.' - '.$row->pegNama.'</option>';
					}
				}
            }			
		}
		else 
           message('Ooops!! Something Wrong!!','error');
	}

	public function loadjabatan()
	{		
		$session_data = $this->session->userdata('logged_in');

		$this->form_validation->set_rules('jjabId','jjabId','trim|required|xss_clean');

		if($this->form_validation->run()) 
		{
			if(IS_AJAX)
            {
				$jjabId = $this->input->post('jjabId');

				$data = $this->model_master->get_ref_table('r_jabatan_2017','jabKelas DESC,jabNilai DESC,jabNama',array('jabJpegId'=>$jjabId));	

				if($data!=false) {
					echo '<option value=""></option>';
					foreach($data as $row)
					{
						echo '<option value="'.$row->jabId.'">'.$row->jabNama.'</option>';
					}
				}
            }			
		}
		else 
           message('Ooops!! Something Wrong!!','error');
	}

	public function index()
	{	
		$data = $this->get_master($this->_path_page.$this->_page_index);
		$data['scripts'] = array($this->_path_js.$this->_controller_name,'load_pegawai');
		$data['response_url'] = site_url($this->_controller_name.'/response');
		$this->load->view($this->template, $data);
	}

	public function response()
	{		
		$session_data = $this->session->userdata('logged_in');
		$this->form_validation->set_rules('unitId','unitId','trim|required|xss_clean');
		$this->form_validation->set_rules('pegNIP','pegNIP','trim|required|xss_clean');

		if($this->form_validation->run()) 
		{
			if(IS_AJAX)
            {
				$pegNIP = $this->input->post('pegNIP');

				$data['datas'] = $this->model_pegawaihistujuhbelas->get($pegNIP);
				$key = $this->encryptions->encode($pegNIP,$this->config->item('encryption_key'));;

				$data['judul'] = $this->_judul;
				$data['add_url'] = site_url('pegawaihistujuhbelas/tambah').'/'.$key;
				$data['edit_url'] = site_url('pegawaihistujuhbelas/ubah').'/';
				$data['delete_url'] = site_url('pegawaihistujuhbelas/hapus').'/';

				$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
				$data['susrNama'] = $session_data['susrNama'];

				if(in_array($session_data['susrNama'], $this->_user_grant))
					$data['user_grant'] = true;
				else
					$data['user_grant'] = false;

				$pages = $this->_path_page.'response';
                $this->load->view($pages,$data);
            }				
		}
		else 
			message('Ooops!! Something Wrong!!','error');
	}

	public function tambah()
	{		
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));

		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';	
		$data['status_page'] = 'Tambah';
		$data['pegawai'] = $this->model_master->get_by_id('d_pegawai',array('pegNIP'=>$id));
		$data['jenis_jabat'] = $this->model_master->get_ref_table('r_jenis_pegawai');
		$data['datas'] = $data['jabatan'] = false;	
		$this->load->view($this->template, $data);
	}	
	
	public function ubah()
	{		
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));

		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';	
		$data['status_page'] = 'Ubah';
		$data['jenis_jabat'] = $this->model_master->get_ref_table('r_jenis_pegawai');
		$data['datas'] = $this->model_pegawaihistujuhbelas->get_by_id($id);		
		$data['pegawai'] = $this->model_master->get_by_id('d_pegawai',array('pegNIP'=>$data['datas']->pegNIP));
		$data['jabatan'] = $this->model_master->get_ref_table('r_jabatan_2017','jabKelas DESC,jabNilai DESC,jabNama',array('jabJpegId'=>$data['datas']->jabJpegId));	
		$this->load->view($this->template, $data);
	}	
	
	public function simpan()
	{		
		$session_data = $this->session->userdata('logged_in');

		$this->form_validation->set_rules('pegNIP','pegNIP','trim|required|xss_clean');
		$this->form_validation->set_rules('pegNama','pegNama','trim|required|xss_clean');
		$this->form_validation->set_rules('pegGol','pegGol','trim|required|xss_clean');
		$this->form_validation->set_rules('pegKodeUnitKerja','pegKodeUnitKerja','trim|required|xss_clean');
		$this->form_validation->set_rules('jjab','jjab','trim|required|xss_clean');
		$this->form_validation->set_rules('pegJabId','pegJabId','trim|required|xss_clean');
		$this->form_validation->set_rules('pegTMT','pegTMT','trim|required|xss_clean');
		$this->form_validation->set_rules('pegTMTakhir','pegTMTakhir','trim|xss_clean');

		if($this->form_validation->run())  
		{

			$pegId = $this->input->post('pegId');
			$pegNIP = $this->input->post('pegNIP');
			$pegNama = $this->input->post('pegNama');
			$pegGol = $this->input->post('pegGol');
			$pegKodeUnitKerja = $this->input->post('pegKodeUnitKerja');	
			$jjab = $this->input->post('jjab');			
			$pegJabId = $this->input->post('pegJabId');
			$pegTMT = $this->input->post('pegTMT');
			$pegTMTakhir = $this->input->post('pegTMTakhir');

			$unit = $this->model_master->get_by_id('r_unit',array('unitId'=>$pegKodeUnitKerja));

			$param = array(
				'pegNIP'=>$pegNIP,
				'pegKodeUnitKerja'=>$pegKodeUnitKerja,
				'pegUnitKerja'=>($unit!=false?$unit->unitNama:''),
				'pegJabId'=>$pegJabId,
				'pegTMT'=>$pegTMT,
				'pegTMTakhir'=>$pegTMTakhir
			);

			if(empty($pegId)) 
			{
				$param['pegCreated'] = $session_data['susrNama'];
				$param['pegTimeCreated'] = date('Y-m-d h:i:s');
				$proses = $this->model_master->insert('d_pegawai_history_jabatan_2017',$param);
			} else {
				$param['pegUpdated'] = $session_data['susrNama'];
				$param['pegTimeUpdated'] = date('Y-m-d h:i:s');
				
				$key = array('pegId'=>$pegId);
	        	$proses = $this->model_master->update('d_pegawai_history_jabatan_2017',$param,$key);
			}

			if ($proses) 
			{
				pegawaihisjabbulan($pegNIP);
				message($this->_judul.' Berhasil Disimpan','success');
			}
			else
				message($this->_judul.' Gagal Disimpan','error');
		} else
			message('Ooops!! Something Wrong!!','error');
	}	
	
	public function hapus($id = '')
	{
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$datas = $this->model_pegawaihistujuhbelas->get_by_id($id);	

		$proses = $this->model_master->delete('d_pegawai_history_jabatan_2017',array('pegId'=>$id));
		if ($proses) 
		{
			pegawaihisjabbulan($datas->pegNIP);
			message($this->_judul.' Berhasil Dihapus','success');
		}
		else
			message($this->_judul.' Gagal Dihapus','error');
	}
}
