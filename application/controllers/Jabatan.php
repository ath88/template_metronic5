<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class jabatan extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/jabatan/";
	private $_path_js = "setting/";
	private $_page_index = 'index';
	private $_page_form = 'form';
	private $_controller_name = 'jabatan';
	private $_judul = 'Jabatan';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_master','',TRUE);

		if($this->session->userdata('logged_in')==false) //cek user logged
			redirect('login','refresh');
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_master->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database			

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	
		$datas = $this->model_master->get_ref_table('ref_jabatan');

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}				

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['datas'] = $datas;
		$data['judul'] = $this->_judul;
		
		return $data;
	}

	public function index()
	{		
		$data = $this->get_master($this->_path_page.$this->_page_index);
		$data['scripts'] = array($this->_path_js.$this->_controller_name);
		$data['add_url'] = site_url($this->_controller_name.'/tambah').'/';
		$data['edit_url'] = site_url($this->_controller_name.'/ubah').'/';
		$data['delete_url'] = site_url($this->_controller_name.'/hapus').'/';
		$this->load->view($this->template, $data);
	}
	
	public function tambah()
	{	
		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';	
		$data['status_page'] = 'Tambah';
		$data['datas'] = false;	
		$this->load->view($this->template, $data);
	}

	public function ubah()
	{		
		$data = $this->get_master($this->_path_page.$this->_page_form);	
		$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$data['scripts'] = array($this->_path_js.$this->_controller_name);	
		$data['save_url'] = site_url($this->_controller_name.'/simpan').'/';	
		$data['status_page'] = 'Ubah';
		$data['datas'] = $this->model_master->get_by_id('ref_jabatan',array('jabatanId'=>$keyS));	
		$this->load->view($this->template, $data);
	}
	
	public function simpan()
	{		
		$this->form_validation->set_rules('jabatanUraian','jabatanUraian','trim|required|xss_clean');
		if($this->form_validation->run()) 
		{	
			if(IS_AJAX)
	        {
	        	$jabatanIdOld = $this->input->post('jabatanIdOld');
	        	$jabatanUraian = $this->input->post('jabatanUraian');  	

        		$param = array(
        			'jabatanUraian'=>$jabatanUraian
        		);

	        	if(empty($jabatanIdOld))
	        	{
	        		$proses = $this->model_master->insert('ref_jabatan',$param);
	        	} else {
	        		$key = array('jabatanId'=>$jabatanIdOld);
	        		$proses = $this->model_master->update('ref_jabatan',$param,$key);
	        	}

	        	if($proses)
	        		message($this->_judul.' Berhasil Disimpan','success');
	        	else
	        		message($this->_judul.' Gagal Disimpan','error');
	        }
	    } else {
	    	message('Ooops!! Something Wrong!!','error');
	    }
	}
	
	public function hapus()
	{
		$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$proses = $this->model_master->delete('ref_jabatan',array('jabatanId'=>$id));
		if ($proses) 
			message($this->_judul.' Berhasil Dihapus','success');
		else
			message($this->_judul.' Gagal Dihapus','error');
	}
}
