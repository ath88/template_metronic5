<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
	<i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">
	<!-- BEGIN: Aside Menu -->
<div 
id="m_ver_menu" 
class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " 
m-menu-vertical="1"
m-menu-scrollable="0" m-menu-dropdown-timeout="500"  
>
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<li class="m-menu__item  <?=!isset($breadcrumb) ? 'm-menu__item--active' : ''?>"  aria-haspopup="true" >
				<a  href="<?=base_url();?>home" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-squares"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">
								Dashboard
							</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__section">
				<h4 class="m-menu__section-text">
					Menus
				</h4>
				<i class="m-menu__section-icon flaticon-more-v3"></i>
			</li>
			<?php 
				//print_r($menu);
				$cRow = 0;
				$curMenu = '';
				$susrmdgroupDisplay = '';
				$susrmodulNamaDisplay = '';

				if(isset($breadcrumb)) {
					$susrmdgroupDisplay = $breadcrumb->susrmdgroupDisplay;
					$susrmodulNamaDisplay = $breadcrumb->susrmodulNamaDisplay;
				}

				if($menu!==false)
				{
					foreach($menu as $row)
					{
						if($curMenu!=$row->susrmdgroupDisplay) 
						{
							$statusCurMenu = false;
							$curMenu=$row->susrmdgroupDisplay;
						} else
							$statusCurMenu = true;

						if($statusCurMenu==false)
						{
							if($cRow!=0)
							{
			?>								
					</ul>
				</div>
			</li>
			<?php
							}
			?>
			<li class="m-menu__item  m-menu__item--submenu <?=$row->susrmdgroupDisplay==$susrmdgroupDisplay ? 'm-menu__item--open m-menu__item--expanded' : ''?>" aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a  href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon <?=$row->susrmdgroupIcon?>	"></i>
					<span class="m-menu__link-text">
						<?=$row->susrmdgroupDisplay?>
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
			<?php
						}
			?>
						<li class="m-menu__item <?=$row->susrmodulNamaDisplay==$susrmodulNamaDisplay ? 'm-menu__item--active' : ''?>" aria-haspopup="true" >
							<a  href="/<?=$row->susrmodulNama?>" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									<?=$row->susrmodulNamaDisplay?>
								</span>
							</a>
						</li>
			<?php
						$cRow++;
						if($cRow==count($menu))
						{
			?>
					</ul>
				</div>
			</li>
			<?php
						}
					}
				}
			?>
		</ul>
	</div>
	<!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->