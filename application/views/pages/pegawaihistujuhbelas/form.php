<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<?php $this->load->view('subheader'); ?>
	<div class="m-content">
		<div class="row">
			<div class="col-md-12">
				<div id="response"></div>
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<!--begin::Form-->
					<form action="<?=$save_url?>" method="post" id="form_form" class="m-form m-form--fit m-form--label-align-right">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										FORM <?=strtoupper($status_page.' '.$judul)?>
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="form-group m-form__group">
								<label>
									NIP
								</label>
								<input type="text" name="pegNIP" class="form-control m-input" placeholder="Nomor Induk Pegawai" value="<?=$pegawai!=false?$pegawai->pegNIP:''?>" <?=$pegawai!=false?'readonly':''?>>
								<input type="hidden" name="pegId" value="<?=$datas!=false?$datas->pegId:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Nama
								</label>
								<input type="text" name="pegNama" class="form-control m-input" placeholder="Nama" value="<?=$pegawai!=false?$pegawai->pegNama:''?>" <?=$pegawai!=false?'readonly':''?>>
							</div>
							<div class="form-group m-form__group">
								<label>
									Pangkat/Golongan
								</label>
								<input type="text" name="pegGol" class="form-control m-input" placeholder="Pangkat/Golongan" value="<?=$pegawai!=false?$pegawai->pegGol:''?>" <?=$pegawai!=false?'readonly':''?>>
							</div>
							<div class="form-group m-form__group">
								<label>
									Unit
								</label>
								<select class="form-control m-select2" id="pegKodeUnitKerja" name="pegKodeUnitKerja">
									<option value=""></option>
								<?php 
								foreach($unit as $row)
								{
									$selected = ($datas!==false?$datas->pegKodeUnitKerja==$row->unitId?'selected':'':'');
									echo '<option value="'.$row->unitId.'" '.$selected.'>'.KodeUnit($row->unitKode).' - '.$row->unitNama.'</option>';
								}
								?>
								</select>
							</div>
							<div class="form-group m-form__group">
								<label>
									Jenis Jabatan
								</label>
								<select class="form-control m-select2" id="jjab" name="jjab">
									<option value=""></option>
								<?php 
								foreach($jenis_jabat as $row)
								{
									$selected = ($datas!==false?$row->jpegId==$datas->jabJpegId?'selected':'':'');
									echo '<option value="'.$row->jpegId.'" '.$selected.'>'.$row->jpegNama.'</option>';
								}
								?>
								</select>
							</div>
							<div class="form-group m-form__group">
								<label>
									Jabatan
								</label>
								<select class="form-control m-select2" id="pegJabId" name="pegJabId">
									<option value=""></option>
								<?php 
								if($jabatan!==false) {
									foreach($jabatan as $row)
									{
										$selected = ($datas!==false?$row->jabId==$datas->pegJabId?'selected':'':'');
										echo '<option value="'.$row->jabId.'" '.$selected.'>'.$row->jabNama.'</option>';
									}
								}
								?>
								</select>
							</div>
							<div class="form-group m-form__group">
								<label>
									TMT
								</label>
								<input class="form-control m-input datepick" name="pegTMT" type="text" value="<?=$datas!==false?$datas->pegTMT:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									TMT
								</label>
								<input class="form-control m-input datepick" name="pegTMTakhir" type="text" value="<?=($datas!==false?$datas->pegTMTakhir=='0000-00-00'?'':$datas->pegTMTakhir:'')?>">
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" id="btn_save" class="btn btn-primary">
									Simpan
								</button>
								<button type="reset" class="btn btn-secondary">
									Batal
								</button>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>
</div>