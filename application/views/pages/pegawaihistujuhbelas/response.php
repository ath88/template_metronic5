<!--begin::Portlet-->
<div class="m-portlet">
    <!--begin::Form-->
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    TABEL <?=strtoupper($judul)?>
                </h3>
            </div>
        </div>
        <?php if($user_grant): ?>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$add_url?>" class="btn btn-outline-accent m-btn m-btn--icon">
                        <span>
                            <i class="fa fa-plus"></i>
                            <span>Tambah</span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <?php endif; ?>
    </div>
    <div class="m-portlet__body">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>
                                No
                            </th>
                            <th >
                                 NIP
                            </th>
                            <th >
                                 Nama
                            </th>
                            <th >
                                 Golongan
                            </th>
                            <th >
                                 Unit Kerja
                            </th>
                            <th >
                                 Jabatan
                            </th>
                            <th >
                                 Kelas
                            </th>
                            <th >
                                 TMT
                            </th>
                            <th >
                                 TMT Akhir
                            </th>
                            <th width="90px">
                                 Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if($datas!==false) {
                            $i=1;
                            foreach($datas as $row)
                            {
                                $key = $this->encryptions->encode($row->pegId,$this->config->item('encryption_key'));
                    ?>
                    <tr>
                        <th scope="row">
                             <?=$i++?>
                        </td>
                        <td>
                             <?=$row->pegNIP?>
                        </td>
                        <td>
                             <?=$row->pegNama?>
                        </td>
                        <td>
                             <?=$row->pegGol?>
                        </td>
                        <td>
                             <?=$row->pegUnitKerja.'<br/>'.$row->parent1.' - '.$row->parent2?>
                        </td>
                        <td>
                             <?=$row->jabNama?>
                        </td>
                        <td>
                             <?=$row->jabKelas?>
                        </td>
                        <td>
                             <?=($row->pegTMT=='0000-00-00'?'':DateToIndo($row->pegTMT))?>
                        </td>
                        <td>
                             <?=($row->pegTMTakhir=='0000-00-00'?'':DateToIndo($row->pegTMTakhir))?>
                        </td>
                        <td>
                        <?php if($user_grant): ?>
                            <a href="<?=$edit_url.$key?>" title="Ubah" class="btn btn-outline-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="<?=$delete_url.$key;?>" title="Hapus" class="ts_remove_row btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" id='ts_remove_row<?= $i; ?>'>
                                <i class="fa fa-remove"></i>
                            </a>  
                        <?php 
                            else:
                                echo 'Closed';
                            endif; ?>                                                 
                        </td> 
                    </tr>                                  
                        <?php
                            }
                        } else
                            echo "<tr><td colspan='10'>Data Tidak Ditemukan</td></tr>";
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!--end::Section-->
    </div>
    <!--end::Form-->
</div>
<!--end::Portlet-->
