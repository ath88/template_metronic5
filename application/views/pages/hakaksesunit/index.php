<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<?php $this->load->view('subheader'); ?>
	<div class="m-content">
		<div class="row">
			<div class="col-md-12">
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<!--begin::Form-->
					<form action="<?=$response_url?>" method="post" id="form_submit" class="m-form m-form--fit m-form--label-align-right">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										<?=strtoupper($judul)?>
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="form-group m-form__group">
								<label>
									Hak Akses
								</label>
								<select class="form-control m-select2" id="hakakses" name="hakakses">
									<option value=""></option>
								<?php
								if($hakakses!=false)
								{
									foreach($hakakses as $row)
									{
										echo '<option value="'.$row->sgroupNama.'">'.$row->sgroupNama.' - '.$row->sgroupKeterangan.'</option>';
									}
								}
								?>
								</select>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" id="btn_button" class="btn btn-primary">
									Tampil
								</button>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
				<div id="response"></div>
			</div>
		</div>
	</div>
</div>