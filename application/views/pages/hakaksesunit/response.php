<!--begin::Portlet-->
<div class="m-portlet">
	<!--begin::Form-->
	<form action="<?=$save_url?>" method="post" id="form_form">
		<input type="hidden" name="sgroupNama" value="<?=!empty($sgroupNama)?$sgroupNama:''?>">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					TABEL <?=strtoupper($judul)?>
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<button type="submit" id="btn_save" class="btn btn-accent">
						Perbaharui
					</button>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">
		<!--begin::Section-->
		<div class="m-section">
			<div class="m-section__content">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>
								#
							</th>
							<th>
								Kode
							</th>
							<th>
								Unit Kerja
							</th>
						</tr>
					</thead>
					<tbody>
					<?php
                        if($datas!==false) {
                            $i=1;
                            foreach($datas as $row)
                            {			
								if (in_array($row->unitId, $unitakses))
									$cek = "checked";
								 else
									$cek=""; 																				
                    ?>
                    <tr>
                        <th scope="row">
                             <input type="checkbox" class="checked" <?=$cek?> name="cekModul[]" value="<?=$row->unitId?>"/>
                        </td>
                        <td>
                             <?=KodeUnit($row->unitKode)?>
                        </td>
                        <td>
                             <?=$row->unitNama?>
                        </td>
                    </tr>                                  
                        <?php
                        
                                $i++;
                                    }
                        } else
                            echo "<tr><td colspan='3'>Data Tidak Ditemukan</td></tr>";
                    ?>
					</tbody>
				</table>
			</div>
		</div>
		<!--end::Section-->
	</div>
	</form>
	<!--end::Form-->
</div>
<!--end::Portlet-->