<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<?php $this->load->view('subheader'); ?>
	<div class="m-content">
		<div class="row">
			<div class="col-md-12">
				<div id="response"></div>
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<!--begin::Form-->
					<form action="<?=$save_url?>" method="post" id="form_form" class="m-form m-form--fit m-form--label-align-right">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										FORM <?=strtoupper($judul)?>
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="form-group m-form__group">
								<label>
									Username
								</label>
								<input type="text" name="susrNama" class="form-control m-input" placeholder="Username" value="<?=$susrNama!=false?$susrNama:''?>" readonly>
							</div>
							<div class="form-group m-form__group">
								<label>
									Name
								</label>
								<input type="text" name="susrProfil" class="form-control m-input" placeholder="Name" value="<?=$susrProfil!=false?$susrProfil:''?>" readonly>
							</div>
							<div class="form-group m-form__group">
								<label>
									Hak Akses
								</label>
								<select class="form-control m-select2" name="HAKAKSES">
									<option value=""></option>
								<?php 
								foreach($hakakses as $row)
								{
									echo '<option value="'.$row->sgroupNama.'" >'.$row->sgroupNama.' - '.$row->sgroupKeterangan.'</option>';
								}
								?>
								</select>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" id="btn_save" class="btn btn-primary">
									Simpan
								</button>
								<button type="reset" class="btn btn-secondary">
									Batal
								</button>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>
</div>