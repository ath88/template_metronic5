<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title ">
					Dashboard
				</h3>
			</div>
			<div>
				<span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
					<span class="m-subheader__daterange-label">
						<span class="m-subheader__daterange-title"></span>
						<span class="m-subheader__daterange-date m--font-brand"></span>
					</span>
					<a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
						<i class="la la-angle-down"></i>
					</a>
				</span>
			</div>
		</div>
	</div>
	<!-- END: Subheader -->
	<div class="m-content">
		
	<!--Begin::Section-->
		<div class="m-portlet">
			<div class="m-portlet__body  m-portlet__body--no-padding">
				<div class="row m-row--no-padding m-row--col-separator-xl">
					<div class="col-xl-12">
						<!--begin:: Widgets/Profit Share-->
						<div class="m-widget14">
							<div class="m-widget14__header">
								<h3 class="m-widget14__title">
									Pegawai
								</h3>
								<span class="m-widget14__desc">
									Tenaga Pendidik dan Tenaga Kependidikan
								</span>
							</div>
							<div class="row  align-items-center">
								<div class="col">
									<div id="m_chart_profit_share" class="m-widget14__chart" style="height: 160px">
										<div class="m-widget14__stat">
											<?=$datas!=false?$datas->DS+$datas->DT+$datas->TKS+$datas->TKF:0?>
										</div>
									</div>
								</div>
								<div class="col">
									<div class="m-widget14__legends">
										<div class="m-widget14__legend">
											<span class="m-widget14__legend-bullet m--bg-accent"></span>
											<span class="m-widget14__legend-text" tagvalue="<?=$datas!=false?$datas->DS:0?>" id="DS">
												<?=$datas!=false?$datas->DS:0?> Dosen Fungsional
											</span>
										</div>
										<div class="m-widget14__legend">
											<span class="m-widget14__legend-bullet m--bg-warning"></span>
											<span class="m-widget14__legend-text" tagvalue="<?=$datas!=false?$datas->DT:0?>" id="DT">
												<?=$datas!=false?$datas->DT:0?> Dosen Tugas Tambahan
											</span>
										</div>
										<div class="m-widget14__legend">
											<span class="m-widget14__legend-bullet m--bg-brand"></span>
											<span class="m-widget14__legend-text" tagvalue="<?=$datas!=false?$datas->TKS:0?>" id="TKS">
												<?=$datas!=false?$datas->TKS:0?> Tenaga Kependidikan Struktural
											</span>
										</div>
										<div class="m-widget14__legend">
											<span class="m-widget14__legend-bullet m--bg-success"></span>
											<span class="m-widget14__legend-text" tagvalue="<?=$datas!=false?$datas->TKF:0?>" id="TKF">
												<?=$datas!=false?$datas->TKF:0?> Tenaga Kependidikan Fungsional
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--end:: Widgets/Profit Share-->
					</div>
				</div>
			</div>
		</div>
		<!--End::Section-->
	</div>
</div>