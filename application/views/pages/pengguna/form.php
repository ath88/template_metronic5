<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<?php $this->load->view('subheader'); ?>
	<div class="m-content">
		<div class="row">
			<div class="col-md-12">
				<div id="response"></div>
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<!--begin::Form-->
					<form action="<?=$save_url?>" method="post" id="form_form" class="m-form m-form--fit m-form--label-align-right">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										FORM <?=strtoupper($status_page.' '.$judul)?>
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="form-group m-form__group">
								<label>
									Username
								</label>
								<input type="text" name="susrNama" class="form-control m-input" placeholder="Username" value="<?=$datas!=false?$datas->susrNama:''?>" <?=$datas!=false?'readonly':''?>>
								<input type="hidden" name="susrNamaOld" value="<?=$datas!=false?$datas->susrNama:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Profil
								</label>
								<input type="text" name="susrProfil" class="form-control m-input" placeholder="Profil" value="<?=$datas!=false?$datas->susrProfil:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Hak Akses
								</label>
								<select class="form-control m-select2" id="susrSgroupNama" name="susrSgroupNama">
									<option value=""></option>
								<?php
								if($hakakses!=false)
								{
									foreach($hakakses as $row)
									{
										$selected='';
										if($datas!=false)
										{
											if($datas->susrSgroupNama==$row->sgroupNama)
												$selected='selected';
										}
										echo '<option value="'.$row->sgroupNama.'" '.$selected.'>'.$row->sgroupNama.' - '.$row->sgroupKeterangan.'</option>';
									}
								}
								?>
								</select>
							</div>
							<div class="form-group m-form__group" <?=$datas!=false?'style="display: none;"':''?>>
								<label>
									Password
								</label>
								<input type="text" name="susrPassword" class="form-control m-input readonly" placeholder="Password" value="<?=$password?>" readonly>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" id="btn_button" class="btn btn-primary">
									Simpan
								</button>
								<button type="reset" class="btn btn-secondary">
									Batal
								</button>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>
</div>