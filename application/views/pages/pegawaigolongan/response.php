<!--begin::Portlet-->
<div class="m-portlet">
    <!--begin::Form-->
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    TABEL <?=strtoupper($judul)?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$add_url?>" class="btn btn-outline-accent m-btn m-btn--icon">
                        <span>
                            <i class="fa fa-plus"></i>
                            <span>Tambah</span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>
                                No
                            </th>
                            <th >
                                 NIP
                            </th>
                            <th >
                                 Nama
                            </th>
                            <th >
                                 Pangkat/Golongan
                            </th>
                            <th >
                                 Nomor, Tanggal SK
                            </th>
                            <th >
                                 TMT
                            </th>
                            <th>
                                 Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if($datas!==false) 
                        {
                            $i=1;
                            foreach($datas as $row)
                            {
                                $key = $this->encryptions->encode($row->pegGolId,$this->config->item('encryption_key'));
                                $filename = './assets/upload_file/'.$row->pegGolNomorKtp.'/'.$row->pegGolTanggalSK.'_'.str_replace('/', '', $row->pegGolGolonganKode).'_2.pdf';
                                if(file_exists($filename))
                                    $download = true;
                                else
                                    $download = false;
                    ?>
                    <tr>
                        <th scope="row">
                             <?=$i++?>
                        </td>
                        <td>
                             <?=$row->pegNip?>
                        </td>
                        <td>
                             <?=(!empty($row->pegGelarDepan)?$row->pegGelarDepan.'. ':'').$row->pegNama.(!empty($row->pegGelarBelakang)?', '.$row->pegGelarBelakang:'')?>
                        </td>
                        <td>
                             <?=$row->golonganKode.' - '.$row->golonganUraian?>
                        </td>
                        <td>
                             <?=$row->pegGolNoSK.', '.DateToIndo($row->pegGolTanggalSK)?>
                        </td>
                        <td>
                             <?=DateToIndo($row->pegGolTmt)?>
                        </td>
                        <td width="120px" style="text-align: center">
                            <a href="<?=$edit_url.$key?>" title="Ubah" class="btn btn-outline-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="<?=$delete_url.$key;?>" title="Hapus" class="ts_remove_row btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" id='ts_remove_row<?= $i; ?>'>
                                <i class="fa fa-remove"></i>
                            </a>             
                            <?php if($download): ?>
                            <a href="<?=site_url('/assets/upload_file/'.$row->pegGolNomorKtp).'/'.$row->pegGolTanggalSK.'_'.str_replace('/', '', $row->pegGolGolonganKode).'_2.pdf'?>" title="Download Excel" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                                <i class="fa fa-download"></i>
                            </a>                   
                            <?php endif; ?> 
                        </td> 
                    </tr>                                  
                        <?php
                            }
                        } else
                            echo "<tr><td colspan='7'>Data Tidak Ditemukan</td></tr>";
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!--end::Section-->
    </div>
    <!--end::Form-->
</div>
<!--end::Portlet-->