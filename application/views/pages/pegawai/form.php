<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<?php $this->load->view('subheader'); ?>
	<div class="m-content">
		<div class="row">
			<div class="col-md-12">
				<div id="response"></div>
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<!--begin::Form-->
					<form action="<?=$save_url?>" method="post" id="form_form" class="m-form m-form--fit m-form--label-align-right">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										FORM <?=strtoupper($status_page.' '.$judul)?>
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="form-group m-form__group">
								<label>
									NIP
								</label>
								<input type="text" name="pegNip" class="form-control m-input" placeholder="Nomor Induk Pegawai" value="<?=$datas!=false?$datas->pegNip:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Nomor KTP
								</label>
								<input type="text" name="pegNomorKtp" class="form-control m-input" placeholder="Nomor KTP" value="<?=$datas!=false?$datas->pegNomorKtp:''?>" <?=$datas!=false?'readonly':''?>>
								<input type="hidden" name="pegNomorKtpOld" value="<?=$datas!=false?$datas->pegNomorKtp:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Nama
								</label>
								<input type="text" name="pegNama" class="form-control m-input" placeholder="Nama" value="<?=$datas!=false?$datas->pegNama:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Gelar Depan
								</label>
								<input type="text" name="pegGelarDepan" class="form-control m-input" placeholder="Gelar Depan" value="<?=$datas!=false?$datas->pegGelarDepan:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Gelar Belakang
								</label>
								<input type="text" name="pegGelarBelakang" class="form-control m-input" placeholder="Gelar Belakang" value="<?=$datas!=false?$datas->pegGelarBelakang:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Kota Lahir
								</label>
								<input type="text" name="pegKotaLahir" class="form-control m-input" placeholder="Kota Lahir" value="<?=$datas!=false?$datas->pegKotaLahir:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Tanggal Lahir
								</label>
								<input class="form-control m-input datepick" name="pegTanggalLahir" type="text" value="<?=$datas!==false?$datas->pegTanggalLahir:''?>">
							</div>
							<div class="m-form__group form-group">
								<label for="">
									Jenis Kelamin
								</label>
								<div class="m-radio-inline">
									<label class="m-radio">
										<input type="radio" name="pegJenisKelamin" id="pegJenisKelamin1" value="L" <?=($datas!==false?$datas->pegJenisKelamin=='L'?'checked':'':'')?>> Laki - Laki
										<span></span>
									</label>
									<label class="m-radio">
										<input type="radio" name="pegJenisKelamin" id="pegJenisKelamin2" value="P" <?=($datas!==false?$datas->pegJenisKelamin=='P'?'checked':'':'')?>> Perempuan
										<span></span>
									</label>
								</div>
							</div>
							<div class="form-group m-form__group">
								<label>
									Golongan Darah
								</label>
								<input class="form-control m-input" name="pegGolonganDarah" type="text" value="<?=$datas!==false?$datas->pegGolonganDarah:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Agama
								</label>
								<select class="form-control m-select2" id="pegAgmrId" name="pegAgmrId">
									<option value=""></option>
								<?php 
								foreach($agama as $row)
								{
									$selected = ($datas!==false?$datas->pegAgmrId==$row->agmrId?'selected':'':'');
									echo '<option value="'.$row->agmrId.'" '.$selected.'>'.$row->agmrNama.'</option>';
								}
								?>
								</select>
							</div>
							<div class="form-group m-form__group">
								<label>
									Status Pernikahan
								</label>
								<select class="form-control m-select2" id="pegStnkrId" name="pegStnkrId">
									<option value=""></option>
								<?php 
								foreach($statusnikah as $row)
								{
									$selected = ($datas!==false?$datas->pegStnkrId==$row->stnkrId?'selected':'':'');
									echo '<option value="'.$row->stnkrId.'" '.$selected.'>'.$row->stnkrNama.'</option>';
								}
								?>
								</select>
							</div>
							<div class="form-group m-form__group">
								<label>
									Alamat Domisili
								</label>
								<input class="form-control m-input" name="pegAlamat" type="text" value="<?=$datas!==false?$datas->pegAlamat:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Nomor HP
								</label>
								<input class="form-control m-input" name="pegNoHP" type="text" value="<?=$datas!==false?$datas->pegNoHP:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Email
								</label>
								<input class="form-control m-input" name="pegEmail" type="text" value="<?=$datas!==false?$datas->pegEmail:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Unit
								</label>
								<select class="form-control m-select2" id="pegUnitId" name="pegUnitId">
									<option value=""></option>
								<?php 
								foreach($unit as $row)
								{
									$selected = ($datas!==false?$datas->pegUnitId==$row->unitId?'selected':'':'');
									echo '<option value="'.$row->unitId.'" '.$selected.'>'.$row->unitNama.'</option>';
								}
								?>
								</select>
							</div>
							<div class="m-form__group form-group">
								<label for="">
									Pensiun
								</label>
								<div class="m-radio-inline">
									<label class="m-radio">
										<input type="radio" name="pegIsAktif" id="pegIsAktif1" value="1" <?=($datas!==false?$datas->pegIsAktif==1?'checked':'':'')?>> TIDAK
										<span></span>
									</label>
									<label class="m-radio">
										<input type="radio" name="pegIsAktif" id="pegIsAktif2" value="0" <?=($datas!==false?$datas->pegIsAktif==0?'checked':'':'')?>> YA
										<span></span>
									</label>
								</div>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" id="btn_button" class="btn btn-primary">
									Simpan
								</button>
								<button type="reset" class="btn btn-secondary">
									Batal
								</button>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>
</div>