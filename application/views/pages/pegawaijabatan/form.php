<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<?php $this->load->view('subheader'); ?>
	<div class="m-content">
		<div class="row">
			<div class="col-md-12">
				<div id="response"></div>
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<!--begin::Form-->
					<form action="<?=$save_url?>" method="post" id="form_form" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										FORM <?=strtoupper($status_page.' '.$judul)?>
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="form-group m-form__group">
								<label>
									NIP
								</label>
								<input type="text" class="form-control m-input" placeholder="Nomor Induk Pegawai" value="<?=$pegawai!=false?$pegawai->pegNip:''?>" readonly>
								<input type="hidden" name="pegJabId" value="<?=$datas!=false?$datas->pegJabId:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Nama
								</label>
								<input type="text" class="form-control m-input" placeholder="Nama" value="<?=$pegawai!=false?$pegawai->pegNama:''?>" readonly>
							</div>
							<div class="form-group m-form__group">
								<label>
									Nomor KTP
								</label>
								<input type="text" name="pegJabNomorKtp" class="form-control m-input" placeholder="Nomor KTP" value="<?=$pegawai!=false?$pegawai->pegNomorKtp:''?>" readonly>
							</div>
							<div class="form-group m-form__group">
								<label>
									Jabatan
								</label>
								<select class="form-control m-select2" id="pegJabJabatanId" name="pegJabJabatanId">
									<option value=""></option>
								<?php 
								foreach($jabatan as $row)
								{
									$selected = ($datas!==false?$row->jabatanId==$datas->pegJabJabatanId?'selected':'':'');
									echo '<option value="'.$row->jabatanId.'" '.$selected.'>'.$row->jabatanUraian.'</option>';
								}
								?>
								</select>
							</div>
							<div class="form-group m-form__group">
								<label>
									Nomor SK
								</label>
								<input class="form-control m-input" name="pegJabNoSK" type="text" value="<?=$datas!==false?$datas->pegJabNoSK:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Tanggal SK
								</label>
								<input class="form-control m-input datepick" name="pegJabTanggalSK" type="text" value="<?=$datas!==false?$datas->pegJabTanggalSK:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									TMT
								</label>
								<input class="form-control m-input datepick" name="pegJabTMT" type="text" value="<?=$datas!==false?$datas->pegJabTMT:''?>">
							</div>
							<div class="form-group m-form__group">
								<label>
									Berkas SK
								</label>
								<div></div>
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="customFile" name="customFile">
									<label class="custom-file-label" for="customFile">
										Pilih file
									</label>
									<span class="m-form__help">*Hanya File PDF</span>
								</div>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" id="btn_save" class="btn btn-primary">
									Simpan
								</button>
								<button type="reset" class="btn btn-secondary">
									Batal
								</button>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>
</div>