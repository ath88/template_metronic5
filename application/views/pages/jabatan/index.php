<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<?php $this->load->view('subheader'); ?>
	<div class="m-content">
		<div class="row">
			<div class="col-md-12">
				<!--begin::Portlet-->
				<div class="m-portlet" id="response">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									<?=strtoupper($judul)?>
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="<?=$add_url?>" class="btn btn-outline-accent m-btn m-btn--icon">
										<span>
											<i class="fa fa-plus"></i>
											<span>Tambah</span>
										</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						<!--begin::Section-->
						<div class="m-section">
							<div class="m-section__content">
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th>
												No
											</th>
											<th>
												Jabatan
											</th>
											<th>
												Action
											</th>
										</tr>
									</thead>
									<tbody>
									<?php
				                        if($datas!==false) {
				                            $i=1;
				                            foreach($datas as $row)
				                            {
				                                $key = $this->encryptions->encode($row->jabatanId,$this->config->item('encryption_key'));																							
				                    ?>
				                    <tr>
				                        <th scope="row">
				                             <?=$i?>
				                        </td>
				                        <td>
				                             <?=$row->jabatanUraian?>
				                        </td>
				                        <td>
				                        	<a href="<?=$edit_url.$key?>" title="Ubah" class="btn btn-outline-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
												<i class="fa fa-pencil"></i>
											</a>
											<a href="<?=$delete_url.$key;?>" title="Hapus" class="ts_remove_row btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" id='ts_remove_row<?= $i; ?>'>
												<i class="fa fa-remove"></i>
											</a>				                                   
				                        </td> 
				                    </tr>                                   
				                        <?php
				                        
				                                $i++;
				                                    }
				                        } else
				                            echo "<tr><td colspan='3'>Data Tidak Ditemukan</td></tr>";
				                    ?>
									</tbody>
								</table>
							</div>
						</div>
						<!--end::Section-->
					</div>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>
</div>