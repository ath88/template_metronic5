<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
require("system/libraries/nusoap/nusoap.php");
class Serviceclient {

	var $url = "http://kepegawaian.unmul.ac.id/servicesidak/servicesidak.php?wsdl";
	var $urlSIA = "http://100.117.119.48/index.php?wsdl";

	public function getPegawaiByNip($id) {
		$client=new nusoap_client($this->url);
		$data=$client->call("getPegawaiByNIP",array("id"=>$id),"urn:SidakServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
    public function getUnit() {
		$client=new nusoap_client($this->url);
		$data=$client->call("getUnit",array(),"urn:SidakServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
    public function getPegawaiByUnit($id) {
		$client=new nusoap_client($this->url);
		$data=$client->call("getPegawaiByUnit",array("id"=>$id),"urn:SidakServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
    public function getUnitByKodeUnit($id) {
		$client=new nusoap_client($this->url);
		$data=$client->call("getUnitByKodeUnit",array("id"=>$id),"urn:SidakServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
    public function getJabatan() {
		$client=new nusoap_client($this->url);
		$data=$client->call("getJabatan",array(),"urn:SidakServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
    public function getJabatanByKode($id,$id2) {
		$client=new nusoap_client($this->url);
		$data=$client->call("getJabatanByKode",array("kdJab"=>$id,"kdJnsJab"=>$id2),"urn:SidakServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
    public function getPegLHKPN($kdunit,$kdjabatan,$kdjnsjbt) {
		$client=new nusoap_client($this->url);
		$data=$client->call("getPegLHKPN",array('kdunit'=>$kdunit,'kdjabatan'=>$kdjabatan,'kdjnsjbt'=>$kdjnsjbt),"urn:SidakServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }

    public function getAjarDosen($nip) {
    	$smtAktif = $this->getSemesterAktif();
		$client=new nusoap_client($this->urlSIA);
		$data=$client->call("getAjarDosen",array('nip'=>$nip,'nama'=>'','semester'=>$smtAktif[0]['semId']),"urn:SIAServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
	
	public function cariMhsSemuaStatus($data) {
		$client=new nusoap_client($this->urlSIA);
		$data=$client->call("cariMhsSemuaStatus",array('scari'=>$data),"urn:SIAServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
	
	public function getMhsByNim($data) {
		$client=new nusoap_client($this->urlSIA);
		$data=$client->call("getMhsByNim",array('scari'=>$data),"urn:SIAServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
	
	
	public function GetSemester() {
		$client=new nusoap_client($this->urlSIA);
		$data=$client->call("GetSemester",array(),"urn:SIAServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
	
	public function GetSemesterAktif() {
		$client=new nusoap_client($this->urlSIA);
		$data=$client->call("GetSemesterAktif",array(),"urn:SIAServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
	
	
	public function GetShiftDikti() {
		$client=new nusoap_client($this->urlSIA);
		$data=$client->call("GetShiftDikti",array(),"urn:SIAServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
	
	public function GetProdiById($prodiKode) {
		$client=new nusoap_client($this->urlSIA);
		$data=$client->call("GetProdiById",array('id'=>$prodiKode),"urn:SIAServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
	public function daftarCetak($prodiKode,$angkatan,$kelas) {
		$client=new nusoap_client($this->urlSIA);
		$data=$client->call("daftarCetak",array('prodiKode'=>$prodiKode,'angkatan'=>$angkatan,'kelas'=>$kelas),"urn:SIAServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
	public function setNonAktif($nim) {
		$client=new nusoap_client($this->urlSIA);
		$data=$client->call("setNonAktif",array('mhsNiu'=>$nim),"urn:SIAServices");
		$err=$client->getError();
		if($err)
			return $err;
		elseif($data!=null)
			return $data;
		else
			return false;
    }
}