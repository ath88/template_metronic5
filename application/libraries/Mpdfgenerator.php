<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Mpdfgenerator {
	public function generate($html,$filename,$orientation="") //default potrait, option: landscape
	{ 
		ini_set('max_execution_time', 0);
		ini_set('memory_limit','348M');

		if(!empty($orientation))
			$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [215, 330], 'orientation' => 'L']);
		else
			$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [215, 330]]);

		$mpdf->SetHTMLFooter('
		<table width="100%" style="vertical-align: bottom; font-family: serif; 
		    font-size: 8pt; color: #000000; font-style: italic;">
		    <tr>
		        <td width="50%" style="text-align:left">Paraf : __________________</td>
		        <td width="50%" style="text-align:right">{PAGENO} | simkinerja.unmul.ac.id | {nbpg}</td>
		    </tr>
		</table>');
		$mpdf->shrink_tables_to_fit = 1;
		$mpdf->WriteHTML($html);
		$mpdf->Output($filename.'.pdf', 'I');
	}
}
?>