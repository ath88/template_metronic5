<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
require("simple_html_dom.php");
#ini_set(default_charset, "utf-8");
#header('Content-Type: application/json; charset=utf-8');
//header('Content-Type: application/json');

class Googlescholarapi
{
	public function iconv($a,$b,$c)
	{
		return $c;
	}

	public function dom_googlescholar($id_user)
	{
		if(!empty($id_user))
		{
			$html=new simple_html_dom();
			$html->load_file("http://scholar.google.se/citations?user=" . $id_user);
			$data['total_citations'] = $html->find("#gsc_rsb_st td.gsc_rsb_std", 0)->plaintext;
			$data['year_citations'] = $html->find('#gsc_g_x .gsc_g_t');
			$data['score_citations'] = $html->find('#gsc_g_bars .gsc_g_al');
			$publications = false;
			foreach($html->find("#gsc_a_t .gsc_a_tr") as $pub) 
			{
				$publications[]['title'] = $pub->find(".gsc_a_at", 0)->plaintext;
				$publications[]['authors'] = $pub->find(".gs_gray", 0)->plaintext;
				$publications[]['venue'] = $pub->find(".gs_gray", 1)->plaintext;
				$publications[]['citations'] = $pub->find(".gsc_a_ac", 0)->plaintext == "&nbsp;" ? "0" : $pub->find(".gsc_a_ac", 0)->plaintext;
				$publications[]['citations'] = $pub->find(".gsc_a_h", 0)->plaintext == " " ? "-" : $pub->find(".gsc_a_h", 0)->plaintext;
			}
			$data['publications'] = $publications;
			return $data;
		} else
			return false;
	}
}
?>
